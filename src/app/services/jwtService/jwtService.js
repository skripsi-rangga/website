import axios from 'axios';
import api from '@api';
import jwtDecode from 'jwt-decode';
import FuseUtils from '@fuse/FuseUtils';

class jwtService extends FuseUtils.EventEmitter {

    init()
    {
        // this.setInterceptors();
        this.handleAuthentication();
    }

    // setInterceptors = () => {
    //     axios.interceptors.response.use(response => {
    //         return response;
    //     }, err => {
    //         return new Promise((resolve, reject) => {
    //             if ( err.response.status === 401 && err.config && !err.config.__isRetryRequest )
    //             {
    //                 // if you ever get an unauthorized response, logout the user
    //                 this.emit('onAutoLogout', 'Invalid access_token');
    //                 this.setSession(null);
    //             }
    //             throw err;
    //         });
    //     });
    // };

    handleAuthentication = () => {

        let accessToken = this.getAccessToken();

        if ( this.isAuthTokenValid(accessToken) )
        // if (accessToken)
        {
            this.setSession(accessToken);
            console.log('accessToken', accessToken) 
            // this.emit('onAutoLogin', true);
            this.emit('onAutoLogin', this.getUserData());
        }
        else
        {
            this.setSession(null);
            this.emit('onAutoLogout', 'access_token expired');
        }
    };

    createUser = (data) => {
        return new Promise((resolve, reject) => {
            axios.post('/api/auth/register', data)
                .then(response => {
                    if ( response.data)
                    {
                        this.setSession(response.data.data.accessToken);
                        resolve(response.data);
                    }
                    else
                    {
                        reject(response.data.error);
                    }
                });
        });
    };

    signInWithEmailAndPassword = (email, password) => {
        return new Promise((resolve, reject) => {
            api.post(process.env.REACT_APP_AUTH_PATH, {
                    email,
                    password
            }).then(response => {

                console.log("RESPONSE", response)
                
                if ( response.data.data.accessToken !== undefined ){
                    this.setSession(response.data.data.accessToken);
                    resolve(this.populateUserData(response.data.data.user.name));
                } else {
                    
                    const message = 'Username atau password yang anda masukkan salah';
                    this.emit('onError', message);
                }
            }).catch(
                (e)=>console.log('error is',e));
        });
    };

    populateUserData = username => {
        const dataUser = {
            role: 'admin',
            data: {
                displayName: username || 'Hello',
                photoURL: 'assets/images/avatars/Velazquez.jpg',
            },
            }
        localStorage.setItem('user_data', JSON.stringify(dataUser));
    
        return {
            role: 'admin',
            data: {
            displayName: username || 'Hello',
            photoURL: 'assets/images/avatars/Velazquez.jpg',
            },
        };
        };

    getUserData = () => {
        const userData = localStorage.getItem('user_data');
        if (userData) return JSON.parse(userData);
        return null;
    };

    signInWithToken = () => {
        return new Promise((resolve, reject) => {
            axios.get('/api/auth/access-token', {
                data: {
                    accessToken: this.getAccessToken()
                }
            })
                .then(response => {
                    if ( response.data.user )
                    {
                        this.setSession(response.data.data.accessToken);
                        resolve(response.data);
                    }
                    else
                    {
                        reject(response.data.error);
                    }
                });
        });
    };

    updateUserData = (user) => {
        return axios.post('/api/auth/user/update', {
            user: user
        });
    };

    setSession = accessToken => {
        if ( accessToken )
        {
            localStorage.setItem('jwt_access_token', accessToken);
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + accessToken;
            api.defaults.headers.common['Authorization'] = 'Bearer ' + accessToken;
        }
        else
        {
            localStorage.removeItem('jwt_access_token');
            localStorage.removeItem('user_data');
            localStorage.removeItem('MP_USER');
            localStorage.removeItem('MP_TOKEN');
            delete axios.defaults.headers.common['Authorization'];
        }
    };

    logout = () => {
        this.setSession(null);
    };

    isAuthTokenValid = accessToken => {
        if ( !accessToken )
        {
            return false;
        }
        const decoded = jwtDecode(accessToken);
        const currentTime = Date.now() / 1000;
        if ( decoded.exp < currentTime )
        {
            return false;
        }
        else
        {
            return true;
        }
    };

    getAccessToken = () => {
        // console.log('access token is',window.localStorage.getItem('jwt_access_token'))
        return window.localStorage.getItem('jwt_access_token');
    };
}

const instance = new jwtService();

export default instance;
