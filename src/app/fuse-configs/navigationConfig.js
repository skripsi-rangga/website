import {authRoles} from 'app/auth';

const navigationConfig = [
    
    {
        'id'      : 'transaction2',
        'title'   : 'Data',
        'type'    : 'group',
        'icon'    : 'dashboard',
        'children': [
          {
            'id'      : 'master-data1',
            'title'   : 'Upload Tunjangan Kinerja',
            'type'    : 'item',
            'icon'    : 'library_books',
            'url'     : '/apps/upload',
          },
          {
            'id'      : 'master-data2',
            'title'   : 'List Tunjangan Kinerja',
            'type'    : 'item',
            'icon'    : 'library_books',
            'url'     : '/apps/list',
          },
          {
            'id'      : 'master-data3',
            'title'   : 'Dokumen Tunjangan Kinerja',
            'type'    : 'item',
            'icon'    : 'library_books',
            'url'     : '/apps/dokumen',
          },
          {
            'id'      : 'master-data5',
            'title'   : 'List Data Pegawai',
            'type'    : 'item',
            'icon'    : 'library_books',
            'url'     : '/apps/pegawai',
          },

        ]
    },
    {
        'id'      : 'auth',
        'title'   : 'Settings',
        'type'    : 'group',
        'icon'    : 'apps',
        'children': [
          {
            'id'   : 'changePassword',
            'title': 'Ubah Data Profile',
            'type' : 'item',
            'url'  : '/pages/change-password',
            'icon' : 'security'
        },
            {
                'id'   : 'changePassword',
                'title': 'Change Password',
                'type' : 'item',
                'url'  : '/pages/change-password',
                'icon' : 'security'
            },
            {
                'id'   : 'logout',
                'title': 'Logout',
                'type' : 'item',
                'url'  : '/logout',
                auth   : authRoles.user,
                'icon' : 'lock'
            },
        ]
    },
];

export default navigationConfig;
