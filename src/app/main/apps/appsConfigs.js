
import { UploadAppConfig } from  './upload/UploadAppConfig';
import { ListAppConfig } from  './list/ListAppConfig';
import { DokumenAppConfig } from  './dokumen/DokumenAppConfig';
import { PegawaiAppConfig } from  './pegawai/PegawaiAppConfig';

export const appsConfigs = [
    UploadAppConfig,
    ListAppConfig,
    DokumenAppConfig,
    PegawaiAppConfig,
];
