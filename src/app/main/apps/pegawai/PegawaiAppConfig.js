import React from 'react';
import {FuseLoadable} from '@fuse';
import {Redirect} from 'react-router-dom';

export const PegawaiAppConfig = {
    settings: {
        layout: {}
    },
    routes  : [
        {
            path     : '/apps/pegawai',
            component: FuseLoadable({
                loader: () => import('./pegawais/Pegawais')
            })
        },
        {
            path     : '/apps/pegawai',
            component: () => <Redirect to="/apps/pegawai/pegawais"/>
        }
    ]
};