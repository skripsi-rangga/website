/*eslint-disable no-unused-vars*/
import React, { Component } from "react";
import { Button, Typography, MenuItem, Paper, Grid, Icon } from "@material-ui/core";
import { FuseAnimate, SelectFormsy, FusePageSimple } from "@fuse";
import { Link } from "react-router-dom";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import api from "@api";
import Formsy from "formsy-react";
import { TextFieldFormsy } from "@fuse";
import { FuseScrollbars } from "@fuse";
import moment from "moment";
import _ from "@lodash";

import { Dialog, DialogTitle, DialogContent, LinearProgress } from "@material-ui/core";
import { TablePagination } from "@material-ui/core";

class Lists extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataList: [],
            loading: false,
            orderBy: "",
            sortBy: "",
            page: 0,
            size: 20,
            name: "",
            period: "",
            nip: "",
            filterState: false,
            periodList:[],
        };
        this.changePage = this.changePage.bind(this);
    }

    componentDidMount() {
        this.fetchDetail();
        this.fetchPeriod();
    }

    searchingAndFiltering = (model) => {
        this.fetchDetail();
    };

    handleClick = (item) => {
        this.props.history.push({
            pathname: "/apps/master/detail/" + item.uuid,
            data: item,
        });
    };

    componentDidUpdate(prevProps, prevState) {
        if (!_.isEqual(this.state.page, prevState.page)) {
            this.fetchDetail();
        }
        if (prevState.name !== this.state.name) {
            this.fetchDetail();
        }
        if (prevState.nip !== this.state.nip) {
            this.fetchDetail();
        }
        if (prevState.period !== this.state.period) {
            this.fetchDetail();
        }
        if (prevState.orderBy !== this.state.orderBy) {
            this.fetchDetail();
        }
        if (prevState.sortBy !== this.state.sortBy) {
            this.fetchDetail();
        }
    }

    changePage(event, page) {
        this.setState({
            page,
        });
    }

    fetchDetail() {
        const { page, size, sortBy, orderBy, name, nip, period } = this.state;
        this.setState({
            loading: true,
        });
        api.get("api/tunkir?", {
            params: {
                orderBy: orderBy,
                sortBy: sortBy,
                page: page + 1,
                size: size,
                name: name,
                nip: nip,
                period: period,
            },
            data: {},
        })
            .then((res) => {
                console.log(res.data.data);
                this.setState({
                    dataList: res.data.data,
                    dictionaries: res.data.dictionaries,
                    pageRequest: res.data.dictionaries.paging.pageRequest,
                    sizeRequest: res.data.dictionaries.paging.sizeRequest,
                    totalElement: res.data.dictionaries.paging.totalElement,
                    totalPage: res.data.dictionaries.paging.totalPage,
                    loading: false,
                });
            })
            .catch((err) => {
                this.setState({
                    loading: false,
                });
            });
    }

    fetchPeriod() {
        api.get("api/tunkir/period", {
            params: {
            },
            data: {},
        })
            .then((res) => {
                this.setState({
                    periodList: res.data.data,
                });
            })
            .catch((err) => {
            });
    }

    onChangeName(name) {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
        this.timeout = setTimeout(() => {
            this.setState({ name });
        }, 500);
    }

    onChangeOrderBy(orderBy) {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
        this.timeout = setTimeout(() => {
            this.setState({ orderBy });
        }, 300);
    }

    onChangeSortBy(sortBy) {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
        this.timeout = setTimeout(() => {
            this.setState({ sortBy });
        }, 300);
    }

    filterData = () => {
        this.setState({
            filterState: true,
        });
    };

    onChangeNamaPegawai(name) {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
        this.timeout = setTimeout(() => {
            this.setState({ name });
        }, 500);
    }

    onChangeNIP(nip) {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
        this.timeout = setTimeout(() => {
            this.setState({ nip });
        }, 500);
    }

    handleCloseFilter = () => {
        this.setState({
            filterState: false,
        });
    };

    handleChangePeriode = (event) => {
        this.setState({
            period: event.target.value,
        });
    };

    formatRupiah=(angka, prefix)=>{
        let number_string = angka && angka.toString(),
        split   		= number_string && number_string.split(','),
        sisa     		= split && split[0].length % 3,
        rupiah     		= split && split[0].substr(0, sisa),
        ribuan     		= split && split[0].substr(sisa).match(/\d{3}/gi);
       
        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
          let separator = sisa ? '.' : '';
          rupiah += separator + ribuan.join('.');
        }
       
        rupiah = split && split[1] !== undefined ? rupiah + ',' + split && split[1] : rupiah;
        return prefix === undefined ? rupiah : (rupiah ? '' + rupiah : '');
      }
      
    render() {
        const { dataList, loading, sortBy, orderBy, dictionaries, pageRequest, sizeRequest, totalElement, page } = this.state;
        return (
            <FusePageSimple
                classes={{
                    toolbar: "min-h-48 h-48",
                    rightSidebar: "w-288",
                }}
                header={
                    <div className="flex flex-col justify-between flex-1 px-24 pt-24">
                        <div className="flex justify-between items-start">
                            <div className="flex items-center max-w-full">
                                <div className="flex flex-col min-w-0">
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography role="button" className="text-16 sm:text-20 truncate">
                                            List Tunjangan Kinerja
                                        </Typography>
                                    </FuseAnimate>
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography variant="caption">deskripsi terkait halaman ini</Typography>
                                    </FuseAnimate>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                content={
                    <div style={{padding:52}}>
                        <Grid container spacing={16}>
                            <Grid item xs={12}>
                                <div className="w-full flex flex-col">
                                    
                                        <Grid container style={{ marginLeft: 16, zIndex:9999 }}>
                                            <Button
                                                style={{ zIndex:9999}}
                                                variant="contained"
                                                color="primary"
                                                value="legacy"
                                                onClick={() => this.filterData()}
                                                size="large"
                                            >
                                                Pencarian
                                            </Button>
                                        </Grid>
                                        <Table>
                                            <TableHead>
                                            <TableRow>{dictionaries !== undefined && <TablePagination rowsPerPageOptions={[pageRequest]} count={totalElement} onChangePage={this.changePage} page={page} rowsPerPage={sizeRequest} />}</TableRow>
                                            </TableHead>
                                        </Table>
                                    
                                </div>
                            </Grid>
                        </Grid>
                        <Grid container spacing={16}>
                            <Grid item xs={12}>
                                <div className="w-full flex flex-col">
                                    <FuseScrollbars className="flex-grow overflow-x-auto">
                                        <Table size="small" style={{  }}  className="min-w-xl" aria-labelledby="tableTitle">
                                            <TableHead>
                                                <TableRow style={{ backgroundColor: "#0064D2", color: "#FFFFFF" }}>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }} rowSpan={2}>
                                                        No
                                                    </TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center", minWidth: 190 }} rowSpan={2}>
                                                        Periode
                                                    </TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center", minWidth: 190 }} rowSpan={2}>
                                                        Nama
                                                    </TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }} rowSpan={2}>
                                                        NIP
                                                    </TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }} rowSpan={2}>
                                                        Jabatan
                                                    </TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }} rowSpan={2}>
                                                        Grade
                                                    </TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }} rowSpan={2}>
                                                        Status
                                                    </TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }} rowSpan={2}>
                                                        Tunkir
                                                    </TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }} rowSpan={2}>
                                                        Hari Kerja
                                                    </TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }} rowSpan={2}>
                                                        Hadir
                                                    </TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }} rowSpan={2}>
                                                        Aplha
                                                    </TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }} rowSpan={2}>
                                                        DL
                                                    </TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center", textAlign: "center" }} colSpan={2}>
                                                        izin
                                                    </TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center", textAlign: "center" }} colSpan={6}>
                                                        Cuti
                                                    </TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center", textAlign: "center" }} colSpan={4}>
                                                        Telat
                                                    </TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center", textAlign: "center" }} colSpan={4}>
                                                        Cepat Pulang
                                                    </TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center", textAlign: "center" }} colSpan={3}>
                                                        Jurnal
                                                    </TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }} rowSpan={2}>
                                                        Ganti
                                                    </TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center", textAlign: "center" }} colSpan={2}>
                                                        Potongan
                                                    </TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }} rowSpan={2}>
                                                        Diterima
                                                    </TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }} rowSpan={2}>
                                                        Potongan PPH
                                                    </TableCell>
                                                </TableRow>
                                                <TableRow style={{ backgroundColor: "#0064D2", color: "#FFFFFF" }}>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center", borderLeft: "1px solid #FFFFFF" }}>Telat</TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }}>Pulang</TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", borderLeft: "1px solid #FFFFFF" }}>Tahun</TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }}>Sakit</TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }}>Hamil</TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }}>Penting</TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }}>Besar</TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }}>Lainnya</TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", borderLeft: "1px solid #FFFFFF" }}>30</TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }}>60</TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }}>90</TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }}>90+</TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center", borderLeft: "1px solid #FFFFFF" }}>30</TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }}>60</TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }}>90</TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }}>90+</TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center", borderLeft: "1px solid #FFFFFF" }}>2</TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center" }}>1</TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center", borderRight: "1px solid #FFFFFF" }}>0</TableCell>

                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center", borderLeft: "1px solid #FFFFFF" }}>Persentase</TableCell>
                                                    <TableCell style={{ color: "#FFFFFF", padding: "4px 10px 4px 10px", textAlign: "center", borderRight: "1px solid #FFFFFF" }}>Nilai</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {this.state.dataList
                                                    ? this.state.dataList.map((n, index) => {
                                                          return (
                                                              <TableRow className="h-64 cursor-pointer" hover key={n.no}>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{index + 1}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>
                                                                      {n.start_period} - {n.end_period} {n.year}
                                                                  </TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px" }}>{n.nama}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.nip}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px" }}>{n.jabatan}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.grade}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.status}</TableCell>


                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{this.formatRupiah(n.tunkir, '') || ' - '}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.harikerja}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.hadir}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.aplha}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.dl}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.izin_telat}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.izin_pulang}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.cuti_tahun}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.cuti_sakit}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.cuti_hamil}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.cuti_penting}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.cuti_besar}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.cuti_lainnya}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.telat_30}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.telat_60}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.telat_90a}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.telat_90b}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.cepat_pulang_30}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.cepat_pulang_60}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.cepat_pulang_90a}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.cepat_pulang_90b}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.jurnal_2}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.jurnal_1}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.jurnal_0}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.ganti}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.potongan_persen}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{n.potongan_nilai}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{this.formatRupiah(n.diterima, '') || ' - '} {}</TableCell>
                                                                  <TableCell style={{ padding: "4px 10px 4px 10px", textAlign: "center" }}>{this.formatRupiah(n.potongan_pph, '') || ' - '} {}</TableCell>
                                                              </TableRow>
                                                          );
                                                      })
                                                    : ""}
                                            </TableBody>
                                        </Table>
                                    </FuseScrollbars>
                                </div>
                            </Grid>
                            <Dialog open={loading} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
                                <DialogTitle id="alert-dialog-title">Sedang proses</DialogTitle>
                                <DialogContent>
                                    <LinearProgress className="w-xs" color="secondary" />
                                </DialogContent>
                            </Dialog>
                            <Dialog size="large" 
                            open={this.state.filterState} 
                            onClose={() => this.handleCloseFilter()}
                            aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
                                <DialogTitle id="alert-dialog-title">Pencarian</DialogTitle>
                                <DialogContent>
                                    <Formsy
                                        onValid={this.onValid}
                                        onInvalid={this.onInvalid}
                                        onSubmit={this.onSubmit}
                                    >
                                        <Grid container >
                                            <Grid style={{ justifyContent: "center", lineHeight: "56px", marginTop:20 }} item xs={4} className="pr-24 mb-16">
                                                        Nama Pegawai
                                            </Grid>
                                            <Grid item xs={8} style={{marginTop:20}} className="pr-24 mb-16">
                                                {/* <input class="upload-excel" type="file" id="fileUpload" onchange="Upload()"/> */}

                                                <TextFieldFormsy
                                                    label={"Nama Pegawai"}
                                                    type="text"
                                                    name="namapegawai"
                                                    autoComplete="off"
                                                    variant="outlined"
                                                    placeholder="Masukkan Nama Pegawai"
                                                    fullWidth
                                                    value={this.state.name}
                                                    InputLabelProps={{ shrink: true }}
                                                    onChange={(e) => this.onChangeNamaPegawai(e.target.value)}
                                                />
                                            </Grid>
                                            <Grid style={{ justifyContent: "center", lineHeight: "56px", marginTop:20 }} item xs={4} className="pr-24 mb-16">
                                                        NIP Pegawai
                                            </Grid>
                                            <Grid item xs={8} style={{marginTop:20}} className="pr-24 mb-16">
                                                {/* <input class="upload-excel" type="file" id="fileUpload" onchange="Upload()"/> */}

                                                <TextFieldFormsy
                                                    label={"NIP Pegawai"}
                                                    type="text"
                                                    name="nip"
                                                    autoComplete="off"
                                                    variant="outlined"
                                                    placeholder="Masukkan NIP Pegawai"
                                                    fullWidth
                                                    value={this.state.nip}
                                                    InputLabelProps={{ shrink: true }}
                                                    onChange={(e) => this.onChangeNIP(e.target.value)}
                                                />
                                            </Grid>
                                            <Grid style={{ justifyContent: "center", lineHeight: "56px", marginTop:20 }} item xs={4} className="pr-24 mb-16">
                                                        Pilih Periode
                                            </Grid>
                                            <Grid item xs={8} style={{marginTop:20}} className="pr-24 mb-16">
                                                {/* <input class="upload-excel" type="file" id="fileUpload" onchange="Upload()"/> */}

                                                <SelectFormsy
                                                    onChange={this.handleChangePeriode}
                                                    className="w-full"
                                                    name="period"
                                                    label="Pilih Periode"
                                                    value={this.state.period}
                                                    variant="outlined"
                                                    autoWidth
                                                    InputLabelProps={{ shrink: true }}
                                                    required
                                                >
                                                    {
                                                        this.state.periodList ?
                                                        this.state.periodList.map((n, index) => {
                                                            return(
                                                            <MenuItem key={n.uuid} value={n.uuid}>
                                                                {n.start_period} - {n.end_period} {n.year}
                                                            </MenuItem>
                                                            );
                                                            
                                                        })
                                                        :""
                                                    }
                                                </SelectFormsy>
                                            </Grid>
                                        </Grid>
                                    </Formsy>
                                </DialogContent>
                            </Dialog>
                        </Grid>
                    </div>
                }
                innerScroll
            />
        );
    }
}

export default Lists;
