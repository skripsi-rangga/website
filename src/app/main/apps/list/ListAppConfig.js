import React from 'react';
import {FuseLoadable} from '@fuse';
import {Redirect} from 'react-router-dom';

export const ListAppConfig = {
    settings: {
        layout: {}
    },
    routes  : [
        {
            path     : '/apps/list',
            component: FuseLoadable({
                loader: () => import('./lists/Lists')
            })
        },
        {
            path     : '/apps/list',
            component: () => <Redirect to="/apps/list/lists"/>
        }
    ]
};