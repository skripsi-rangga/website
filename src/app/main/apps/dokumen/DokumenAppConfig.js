import React from 'react';
import {FuseLoadable} from '@fuse';
import {Redirect} from 'react-router-dom';

export const DokumenAppConfig = {
    settings: {
        layout: {}
    },
    routes  : [
        {
            path     : '/apps/dokumen',
            component: FuseLoadable({
                loader: () => import('./dokumens/Dokumens')
            })
        },
        {
            path     : '/apps/dokumen',
            component: () => <Redirect to="/apps/dokumen/dokumens"/>
        }
    ]
};