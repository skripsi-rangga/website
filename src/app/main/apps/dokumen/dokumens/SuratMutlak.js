import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './StyleDokumenSurat.css';
import { Button, Typography, MenuItem, Paper, Grid, Icon } from "@material-ui/core";

class SuratMutlak extends Component {
    render() {
        return (
            <div>
                <style>
                    {`@page {
                        size: A4 portrait
                    }@media print { body {  line-height: 24px;}}
                    `}
                </style>
            <div id="section-to-print custom">
                <div  style={{textAlign:"center", marginBottom:20, lineHeight:1.2}}>
                    <Grid container>
                        <Grid xs={2}>
                            <img width="128" src="assets/images/logo1.png" alt="logo"/>
                        </Grid>
                        <Grid xs={10}>
                        KEMENTRIAN HUKUM DAN HAK ASASI MANUSIA <br/>
                        REPUBLIK INDONESIA<br/>
                        KANTOR WILAYAH DKI JAKARTA<br/>
                        <b>LEMBAGA PEMASYARAKATAN KELAS IIA SALEMBA</b><br/>
                        Jln. Percetakan Negara No. 88 A, Jakarta Pusat 10570<br/>
                        Telepon : (021) 42883804, Faksimile : (021) 42883881<br/>
                        Surel: lpsalemba.dki@gmail.com<br/>
                        </Grid>
                    </Grid>
                    
                    <div>
                        ______________________________________________________________________________________
                    </div>
                   
                </div>
                <div style={{textAlign:"center", fontWeight:"bold"}}>
                SURAT PERNYATAAN TANGGUNG JAWAB MUTLAK <br/>
                Nomor: {this.props.suratMutlak}<br/>
                </div>
              
                <br/>
                Yang bertanda tangan dibawah ini:
                <br/>
                <br/>
                <Grid container style={{marginTop:20}}>
                    <Grid xs={2}>
                        <p>Nama</p>
                        <p>NIP</p>
                        <p>Jabatan</p>
                    </Grid>
                    <Grid xs={10}>
                        <p>: {this.props.namapegawai1}</p>
                        <p>: {this.props.nip1}</p>
                        <p>: Kepala Lembaga Pemasyarakatan Kelas IIA Salemba</p>
                    </Grid>
                </Grid>
                <br/>
                <br/>
                Menyatakan dengan sesungguhnya bahwa: 
                <ol>
                    <li style={{textAlign:"justify"}}>
                    Perhitungan yang terdapat pada Tunjangan Kinerja Bulan {this.props.end_period} Tahun Anggaran {this.props.style} 
                    Pada Satuan Kerja Lembaga Pemasyarakatan Kelas IIA Salemba telah dihitung dengan 
                    benar dan sepenuhnya menjadi tanggung jawab Kepada Satuan Kerja Lembaga Permasyarakatan Kelas IIA Salemba.
                    </li>
                    <li style={{marginTop:10, textAlign:"justify"}}>
                    Apabila dikemudian hari terdapat kelebihan atas pembayaran tunjangan kinerja tersebut kami bersedia untuk menyetor kelebihan tersebut ke Kas Negara melalui Rekening Bendahara Pengeluaran Sekretariat Jenderal.
                    </li>
                </ol>
                <div style={{marginTop:20}}>
                    Demikian pernyataan ini kami buat dengan sebenar-benarnya.
                </div>
                
                <Grid container style={{marginTop:20}}>
                    <Grid xs={8}>

                    </Grid>
                    <Grid xs={4}>
                        Jakarta, {this.props.tanggal} <br/>
                        K E P A L A <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <b>{this.props.namapegawai1}</b><br/>
                        NIP: {this.props.nip1}
                    </Grid>
                </Grid>
            </div>
            </div>
        );
    }
}

SuratMutlak.propTypes = {

};

export default SuratMutlak;