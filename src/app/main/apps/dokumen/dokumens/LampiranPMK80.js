import React, { Component } from "react";
import "./StyleDokumen.css";
import { Button, Typography, MenuItem, Paper, Grid, Icon } from "@material-ui/core";
class LampiranPMK80 extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    formatRupiah = (angka, prefix) => {
        let number_string = angka && angka.toString(),
            split = number_string && number_string.split(","),
            sisa = split && split[0].length % 3,
            rupiah = split && split[0].substr(0, sisa),
            ribuan = split && split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            let separator = sisa ? "." : "";
            rupiah += separator + ribuan.join(".");
        }

        rupiah = split && split[1] !== undefined ? rupiah + "," + split && split[1] : rupiah;
        return prefix === undefined ? rupiah : rupiah ? "" + rupiah : "";
    };

    headerTable = () => {
        return (
            <>
                <tr>
                    <th rowSpan={3} style={{ borderWidth: "1px", padding: 4 }}>
                        No
                    </th>
                    <th rowSpan={3} style={{ borderWidth: "1px", padding: 4 }}>
                        Uraian Kelas Jabatan
                    </th>
                    <th rowSpan={3} style={{ borderWidth: "1px", padding: 4 }}>
                        Jumlah Penerima
                    </th>
                    <th rowSpan={3} style={{ borderWidth: "1px", padding: 4 }}>
                        Tunjangan Kinerja Per Kode Jabatan
                    </th>
                    <th style={{ borderWidth: "1px", padding: 4 }}>
                        <Grid container>
                            <Grid xs={2}>1.</Grid>
                            <Grid xs={10}> Jumlah Tunjangan</Grid>
                        </Grid>
                    </th>
                    <th style={{ borderWidth: "1px", padding: 4 }}>
                        <Grid container>
                            <Grid xs={2}>1.</Grid>
                            <Grid xs={10}> Potongan Pajak</Grid>
                        </Grid>
                    </th>
                </tr>
                <tr>
                    <th style={{ borderWidth: "1px", padding: 4 }}>
                        <Grid container>
                            <Grid xs={2}>2.</Grid>
                            <Grid xs={10}> Pajak</Grid>
                        </Grid>
                    </th>
                    <th rowSpan={2} style={{ borderWidth: "1px", padding: 4 }}>
                        <Grid container>
                            <Grid xs={2}>2.</Grid>
                            <Grid xs={10}> Jumlah Netto</Grid>
                        </Grid>
                    </th>
                </tr>
                <tr>
                    <th style={{ borderWidth: "1px", padding: 4 }}>
                        <Grid container>
                            <Grid xs={2}>3.</Grid>
                            <Grid xs={10}> Jumlah </Grid>
                        </Grid>
                    </th>
                </tr>
            </>
        );
    };

    pagedata = () => {
        var indents = [];

        let totalpenerima=0;
        let total_tunjangan_keseluruhan=0;
        let total_pajak_keseluruhan=0;
        for (let x = 0; x < this.props.resultData.length; x++) {
            let totaltunjangan = 0;
            let totalpajakchildren = 0;
            totalpenerima += this.props.resultData[x].children.length;

            for (let m = 0; m < this.props.resultData[x].children.length; m++) {
                totalpajakchildren += parseFloat(this.props.resultData[x].children[m].potongan_pph);
                total_pajak_keseluruhan += parseFloat(this.props.resultData[x].children[m].potongan_pph);
            }

            for (let c = 0; c < this.props.resultData[x].children.length; c++) {
                totaltunjangan += parseFloat(this.props.resultData[x].children[c].diterima);
                total_tunjangan_keseluruhan +=parseFloat(this.props.resultData[x].children[c].diterima);
            }

            indents.push(
                <>
                    <tr>
                        <td rowSpan={3} style={{ borderWidth: "1px" }}>
                            {x + 1}
                        </td>
                        <td rowSpan={3} style={{ borderWidth: "1px" }}>
                            GRADE {this.props.resultData[x].children[0].grade} {this.props.resultData[x].status === "CPNS" ? "CPNS" : ""}
                        </td>
                        <td rowSpan={3} style={{ borderWidth: "1px" }}>
                            {" "}
                            {this.props.resultData[x].children.length}
                        </td>
                        <td rowSpan={3} style={{ borderWidth: "1px", textAlign: "right" }}>
                            {this.formatRupiah(this.props.resultData[x].tunjangan, "") || ""}
                        </td>
                        <td style={{ borderWidth: "1px", textAlign: "right" }}>
                            <Grid container>
                                <Grid xs={2}>1.</Grid>
                                <Grid xs={10}> {this.formatRupiah(totaltunjangan, "") || "0"}</Grid>
                            </Grid>
                        </td>

                        <td style={{ borderWidth: "1px", textAlign: "right" }}>
                            <Grid container>
                                <Grid xs={2}>1.</Grid>
                                <Grid xs={10}>{this.formatRupiah(totalpajakchildren, "") || "0"}</Grid>
                            </Grid>
                        </td>
                    </tr>
                    <tr>
                        <td style={{ borderWidth: "1px", textAlign: "right" }}>
                            <Grid container>
                                <Grid xs={2}>2.</Grid>
                                <Grid xs={10}>{this.formatRupiah(totalpajakchildren, "") || "0"}</Grid>
                            </Grid>
                        </td>
                        <td style={{ borderWidth: "1px", textAlign: "right" }}>
                            <Grid container>
                                <Grid xs={2}>2.</Grid>
                                <Grid xs={10}>{this.formatRupiah(totaltunjangan, "") || "0"}</Grid>
                            </Grid>
                        </td>
                    </tr>
                    <tr>
                        <td style={{ borderWidth: "1px", textAlign: "right" }}>
                            <Grid container>
                                <Grid xs={2}>3.</Grid>
                                <Grid xs={10}>{this.formatRupiah(totalpajakchildren + totaltunjangan, "") || "0"}</Grid>
                            </Grid>
                        </td>
                        <td style={{ borderWidth: "1px", textAlign: "right" }}></td>
                    </tr>
                </>
            );
        }
        return (
            <>
                {indents}
                <tr>
                        <td rowSpan={3} style={{ borderWidth: "1px" }}>
                           
                        </td>
                        <td rowSpan={3} style={{ borderWidth: "1px" }}>
                            Jumlah
                        </td>
                        <td rowSpan={3} style={{ borderWidth: "1px" }}>
                           {totalpenerima}
                        </td>
                        <td rowSpan={3} style={{ borderWidth: "1px", textAlign: "right" }}>
                            
                        </td>
                        <td style={{ borderWidth: "1px", textAlign: "right" }}>
                            <Grid container>
                                <Grid xs={2}>1.</Grid>
                                <Grid xs={10}> {this.formatRupiah(total_tunjangan_keseluruhan, "") || ""}</Grid>
                            </Grid>
                        </td>

                        <td style={{ borderWidth: "1px", textAlign: "right" }}>
                            <Grid container>
                                <Grid xs={2}>1.</Grid>
                                <Grid xs={10}>{this.formatRupiah(total_pajak_keseluruhan, "") || ""}</Grid>
                            </Grid>
                        </td>
                    </tr>
                    <tr>
                        <td style={{ borderWidth: "1px", textAlign: "right" }}>
                            <Grid container>
                                <Grid xs={2}>2.</Grid>
                                <Grid xs={10}>{this.formatRupiah(total_pajak_keseluruhan, "") || ""}</Grid>
                            </Grid>
                        </td>
                        <td style={{ borderWidth: "1px", textAlign: "right" }}>
                            <Grid container>
                                <Grid xs={2}>2.</Grid>
                                <Grid xs={10}> {this.formatRupiah(total_tunjangan_keseluruhan, "") || ""}</Grid>
                            </Grid>
                        </td>
                    </tr>
                    <tr>
                        <td style={{ borderWidth: "1px", textAlign: "right" }}>
                            <Grid container>
                                <Grid xs={2}>3.</Grid>
                                <Grid xs={10}>{this.formatRupiah(total_pajak_keseluruhan + total_tunjangan_keseluruhan, "") || "0"}</Grid>
                            </Grid>
                        </td>
                        <td style={{ borderWidth: "1px", textAlign: "right" }}></td>
                    </tr>
            </>);
    };

    functionTable = () => {
        if (this.props.number === 1) {
        } else if (this.props.number === 2) {
        } else if (this.props.number === 3) {
        } else if (this.props.number === 4) {
            console.log(this.props.resultData);

            return (
                <>
                    <div id="section-to-print">
                        <div className="header-print" style={{ textAlign: "right", fontWeight: "bold" }}>
                            LAMPIRAN A
                        </div>
                        <div className="title-print" style={{ textAlign: "center", fontWeight: "bold" }}>
                            KEMENTRIAN HUKUM DAN HAM RI <br />
                            LEMBAGA PEMASYARAKATAN KELAS IIA SALEMBA <br />
                            REKAPITULASI DAFTAR PEMBAYARAN TUNJANGAN KINERJA PEGAWAI <br />
                            BULAN {this.props.end_period.toUpperCase()} TAHUN {this.props.year} <br />
                        </div>

                        <table className="tableCustom" border="1">
                            {this.headerTable()}
                            {this.pagedata()}
                        </table>
                    </div>
                </>
            );
        }
    };

    render() {
        return (
            <div>
                <style>
                    {`@page {
                        size: A4 portrait
                    }`}
                </style>
                <div className="no-print">
                    Preview:
                    <br />
                    Tanggal: {this.props.tanggal}
                    <br />
                    Kepala Lapas: {this.props.namapegawai1}
                    <br />
                    Kepala Lapas NIP: {this.props.nip1}
                    <br />
                    Pejabat Pembuat Komitmen: {this.props.namapegawai2}
                    <br />
                    Pejabat Pembuat Komitmen NIP: {this.props.nip2}
                    <br />
                    Bendahara Pengeluaran: {this.props.namapegawai3}
                    <br />
                    Bendahara Pengeluaran NIP: {this.props.nip3}
                    <br />
                </div>

                {this.functionTable()}

                <Grid className="tableCustom" style={{ marginTop: 10 }} container>
                    <Grid item xs={8}>
                        Pejabat Pembuat Komitmen
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <b>{this.props.namapegawai2}</b>
                        <br />
                        NIP: {this.props.nip2}
                    </Grid>
                    <Grid item xs={4}>
                        Jakarta, {this.props.tanggal}
                        <br />
                        Kepala Lapas Kelas IIA Salemba
                        <br />
                        <br />
                        <br />
                        <b>{this.props.namapegawai1}</b>
                        <br />
                        NIP: {this.props.nip1}
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default LampiranPMK80;
