import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './StyleDokumenSurat.css';
import { Button, Typography, MenuItem, Paper, Grid, Icon } from "@material-ui/core";

class SuratUsulan extends Component {
    render() {
        return (
            <div>
                <style>
                {`@page {
                        size: A4 portrait
                    }@media print { body {  line-height: 24px;}}
                    `}
                </style>
            <div id="section-to-print custom">
                <div  style={{textAlign:"center", marginBottom:5, lineHeight:1.2}}>
                    <Grid container>
                        <Grid xs={2}>
                            <img width="128" src="assets/images/logo1.png" alt="logo"/>
                        </Grid>
                        <Grid xs={10}>
                        KEMENTRIAN HUKUM DAN HAK ASASI MANUSIA <br/>
                        REPUBLIK INDONESIA<br/>
                        KANTOR WILAYAH DKI JAKARTA<br/>
                        <b>LEMBAGA PEMASYARAKATAN KELAS IIA SALEMBA</b><br/>
                        Jln. Percetakan Negara No. 88 A, Jakarta Pusat 10570<br/>
                        Telepon : (021) 42883804, Faksimile : (021) 42883881<br/>
                        Surel: lpsalemba.dki@gmail.com<br/>
                        </Grid>
                    </Grid>
                    
                    <div>
                        ______________________________________________________________________________________
                    </div>
                   
                </div>
                <Grid container>
                    <Grid xs={2}>
                        <p>Nomor</p>
                        <p>Sifat</p>
                        <p>Lampiran</p>
                        <p>Hal</p>
                    </Grid>
                    <Grid xs={8}>
                        <p>: {this.props.suratUsulan}</p>
                        <p>: Segera</p>
                        <p>: 1 (Satu) berkas</p>
                        <p>: Usulan Permintaan Pembayaran Tunjangan Kinerja 
                            <br/> <span style={{marginLeft:8}}>Lembaga Pemasyarakatan IIA Salemba</span>
                            <br/> <span style={{marginLeft:8}}>Bulan {this.props.end_period} {this.props.year}</span>
                            </p>
                    </Grid>
                    <Grid xs={2}>
                        <p> {this.props.tanggal}</p>
                    </Grid>
                </Grid>
                <Grid container style={{marginTop:10}}>
                    <Grid xs={1}>
                        <p>Yth.</p>
                    </Grid>
                    <Grid xs={11}>
                        <p>Kepala Kantor Wilayah</p>
                        <p>Kementerian Hukum dan HAM</p>
                        <p>DKI Jakarta</p>
                        <p>Di Jakarta</p>
                    </Grid>
                    
                </Grid>
                <br/>
                <br/>
                <div style={{textAlign:"justify",  textIndent: "20px"}}>
                    Menindaklanjuti Surat Edaran Sekretariat Jenderal Kementrian {this.props.suratKementrian} tentang pelaksanaan pembayaran dan pelaporan tunjangan kinerja bagi menteri dan pegawai di lingkungan Kementrian
                    Hukum dan HAM RI hal tersebut pada rokok surat, bersama ini terlampir usulan permintaan pembayaran tunjangan kinerja pegawai bulan {this.props.end_period} {this.props.year} satker
                    Lembaga Pemasyarakatan Kelas IIA Salemba (497651)
                </div>
                <div style={{marginTop:20}}>
                    Demikian kami sampaikan, atas perhatiannya kami ucapkan terima kasih.
                </div>
                
                <Grid container style={{marginTop:20}}>
                    <Grid xs={8}>

                    </Grid>
                    <Grid xs={4}>
                        Jakarta, {this.props.tanggal} <br/>
                        K E P A L A <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <b>{this.props.namapegawai1}</b><br/>
                        NIP: {this.props.nip1}
                    </Grid>
                </Grid>
            </div>
            </div>
        );
    }
}



export default SuratUsulan;