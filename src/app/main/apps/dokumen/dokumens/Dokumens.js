/*eslint-disable no-unused-vars*/
import React, { Component } from "react";
import { Button, Typography, MenuItem, Paper, Grid, Icon } from "@material-ui/core";
import { FuseAnimate, SelectFormsy, FusePageSimple } from "@fuse";
import { Link } from "react-router-dom";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import api from "@api";
import Formsy from "formsy-react";
import { TextFieldFormsy } from "@fuse";
import { FuseScrollbars } from "@fuse";
import moment from "moment";
import _ from "@lodash";
import { jsPDF } from "jspdf";
import html2canvas from "html2canvas";
import { Dialog, DialogTitle, DialogContent, LinearProgress } from "@material-ui/core";
import { TablePagination } from "@material-ui/core";
import DokumenTandaTerimaTunjangan from "./DokumenTandaTerimaTunjangan";
import RekapitulasiPPH21 from "./RekapitulasiPPH21";
import LampiranPMK80 from "./LampiranPMK80";
import SuratMutlak from "./SuratMutlak";
import SuratUsulan from "./SuratUsulan";

class Dokumens extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataList: [],
            loading: false,
            orderBy: "",
            sortBy: "",
            page: 0,
            size: 20,
            name: "",
            period: "",
            nip: "",
            filterState: false,
            periodList: [],
            dokumen: 0,
            resultData: [],
            totaltunjangan: 0,
            totalpajak: 0,
            totalpotongannilai: 0,
            periodDetail: [],
            namapegawai1: "",
            nip1: "",
            namapegawai2: "",
            nip2: "",
            namapegawai3: "",
            nip3: "",
            tanggal: "",
            suratMutlak:"W.10.PAS.PAS.3.KU.01.02-001",
            suratUsulan:"W.10.PAS.PAS.3.KU.01.02-002",
            suratKementrian:"SEK.3.KU.04.01-03 Tanggal 15 Januari 2021",
            totalPPH:[],
        };
        this.changePage = this.changePage.bind(this);
    }

    componentDidUpdate(prevProps, prevState) {
   
        if (prevState.dokumen !== this.state.dokumen) {
            // this.addExternal();
        }
       
    }

    addExternal = (filename, filetype) => {
       
    }

    printData = () => {
        const printArea = document.getElementById("printWrapper");

        html2canvas(printArea, {
            logging: false,
            useCORS: true,
        }).then((canvas) => {
            const dataURL = canvas.toDataURL();
            const pdf = new jsPDF();
            pdf.addImage(dataURL, "JPEG", 5, 5, 200, 120);
            pdf.save("saved.pdf");
        });
    };

    componentDidMount() {
        this.fetchPeriod();
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.dokumen !== this.state.dokumen) {
            this.fetchDetail();
            this.fetchTotalPPH();
        }
        if (prevState.period !== this.state.period) {
            this.fetchDetail();
            this.fetchTotalPPH();
        }
    }

    changePage(event, page) {
        this.setState({
            page,
        });
    }

    fetchDetail() {
        const { page, size, sortBy, orderBy, name, nip, period } = this.state;
        this.setState({
            loading: true,
        });
        api.get("api/tunkir/dokumen?", {
            params: {
                period: period,
            },
            data: {},
        })
            .then((res) => {
                // console.log(res.data.data)
                this.setState({
                    dataList: res.data.data,
                    loading: false,
                });
            })
            .catch((err) => {
                this.setState({
                    loading: false,
                });
            });
    }

    fetchTotalPPH() {
        const { page, size, sortBy, orderBy, name, nip, period } = this.state;
        this.setState({
            loading: true,
        });
        api.get("api/tunkir/jumlahPPH?", {
            params: {
                period: period,
            },
            data: {},
        })
            .then((res) => {
                console.log("totalPPH :", res.data.data)
                this.setState({
                    totalPPH: res.data.data,
                    loading: false,
                });
            })
            .catch((err) => {
                this.setState({
                    loading: false,
                });
            });
    }

    fetchPeriod() {
        api.get("api/tunkir/period", {
            params: {},
            data: {},
        })
            .then((res) => {
                this.setState({
                    periodList: res.data.data,
                });
            })
            .catch((err) => {});
    }

    handleChangePeriode = (event) => {
        this.setState({
            period: event.target.value,
        });
        this.detailPeriod(event.target.value);
    };

    detailPeriod = (uuid) => {
        

        api.get("api/tunkir/period/detail?", {
            params: {
                period: uuid,
            },
            data: {},
        })
            .then((res) => {
                this.setState({
                    periodDetail: res.data.data,
                    start_period:res.data.data.start_period,
                    end_period:res.data.data.end_period,
                    year:res.data.data.year,
                    loading: false,
                });
            })
            .catch((err) => {
                this.setState({
                    loading: false,
                });
            });
    };

    handleChangeDokumen = (event) => {
        this.setState({
            dokumen: event.target.value,
        });
        this.dokumenCreate(event.target.value);
    };

    dokumenCreate = (number) => {
        const { dataList, dokumen } = this.state;

        if (number === 1) {
            const resultData = dataList.reduce((r, { tunkir: tunjangan, ...object }) => {
                var temp = r.find((o) => o.tunjangan === tunjangan);
                if (!temp) r.push((temp = { tunjangan, children: [] }));
                temp.children.push(object);
                temp.status = object.status;
                temp.grade = object.grade;
                return r;
            }, []);

            resultData.sort((a, b) => parseFloat(b.grade) - parseFloat(a.grade));

            let totaltunjangan = 0;
            let totalpajak = 0;
            let totalpotongannilai = 0;

            for (let x = 0; x < resultData.length; x++) {
                totaltunjangan += parseFloat(resultData[x].tunjangan) * parseFloat(resultData[x].children.length);
            }

            for (let x = 0; x < resultData.length; x++) {
                // totalpajak += parseFloat(resultData[x].children[i].potongan_pph);
                for (let z = 0; z < resultData[x].children.length; z++) {
                    totalpajak += parseFloat(resultData[x].children[z].potongan_pph);
                }
            }

            for (let x = 0; x < resultData.length; x++) {
                for (let z = 0; z < resultData[x].children.length; z++) {
                    totalpotongannilai += parseFloat(resultData[x].children[z].potongan_nilai);
                }
            }

            this.setState({
                resultData,
                totaltunjangan,
                totalpajak,
                totalpotongannilai,
            });
        } else if (number === 2) {
            this.setState({
                resultData: dataList.sort((a, b) => parseFloat(b.grade) - parseFloat(a.grade)),
            });
        } else if (number === 3){
            this.setState({
                resultData: dataList.sort((a, b) => parseFloat(b.grade) - parseFloat(a.grade)),
            });
        } else if (number === 4){
            const resultData = dataList.reduce((r, { tunkir: tunjangan, ...object }) => {
                var temp = r.find((o) => o.tunjangan === tunjangan);
                if (!temp) r.push((temp = { tunjangan, children: [] }));
                temp.children.push(object);
                temp.status = object.status;
                temp.grade = object.grade;
                return r;
            }, []);

            resultData.sort((a, b) => parseFloat(b.grade) - parseFloat(a.grade));

            console.log("resultData", resultData)

            let totaltunjangan = 0;
            let totalpajak = 0;
            let totalpotongannilai = 0;

            for (let x = 0; x < resultData.length; x++) {
                totaltunjangan += parseFloat(resultData[x].tunjangan) * parseFloat(resultData[x].children.length);
            }

            for (let x = 0; x < resultData.length; x++) {
                // totalpajak += parseFloat(resultData[x].children[i].potongan_pph);
                for (let z = 0; z < resultData[x].children.length; z++) {
                    totalpajak += parseFloat(resultData[x].children[z].potongan_pph);
                }
            }

            for (let x = 0; x < resultData.length; x++) {
                for (let z = 0; z < resultData[x].children.length; z++) {
                    totalpotongannilai += parseFloat(resultData[x].children[z].potongan_nilai);
                }
            }

            this.setState({
                resultData,
                totaltunjangan,
                totalpajak,
                totalpotongannilai,
            });
        }
    };

    formatRupiah = (angka, prefix) => {
        let number_string = angka && angka.toString(),
            split = number_string && number_string.split(","),
            sisa = split && split[0].length % 3,
            rupiah = split && split[0].substr(0, sisa),
            ribuan = split && split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            let separator = sisa ? "." : "";
            rupiah += separator + ribuan.join(".");
        }

        rupiah = split && split[1] !== undefined ? rupiah + "," + split && split[1] : rupiah;
        return prefix === undefined ? rupiah : rupiah ? "" + rupiah : "";
    };


   



    onChangeNamaPegawai1(namapegawai1) {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
        this.timeout = setTimeout(() => {
            this.setState({ namapegawai1 });
        }, 500);
    }

    onChangeNamaNip1(nip1) {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
        this.timeout = setTimeout(() => {
            this.setState({ nip1 });
        }, 500);
    }

    onChangeNamaPegawai2(namapegawai2) {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
        this.timeout = setTimeout(() => {
            this.setState({ namapegawai2 });
        }, 500);
    }

    onChangeNamaNip2(nip2) {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
        this.timeout = setTimeout(() => {
            this.setState({ nip2 });
        }, 500);
    }

    onChangeNamaPegawai3(namapegawai3) {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
        this.timeout = setTimeout(() => {
            this.setState({ namapegawai3 });
        }, 500);
    }

    onChangeNamaNip3(nip3) {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
        this.timeout = setTimeout(() => {
            this.setState({ nip3 });
        }, 500);
    }

    onChangeTanggal(tanggal) {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
        this.timeout = setTimeout(() => {
            this.setState({ tanggal });
        }, 500);
    }

    onChangeSuratMutlak(suratMutlak) {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
        this.timeout = setTimeout(() => {
            this.setState({ suratMutlak });
        }, 500);
    }

    onChangeSuratUsulan(suratUsulan) {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
        this.timeout = setTimeout(() => {
            this.setState({ suratUsulan });
        }, 500);
    }

    onChangeSuratKementrian(suratKementrian) {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
        this.timeout = setTimeout(() => {
            this.setState({ suratKementrian });
        }, 500);
    }

    

    returnPage = () => {
        if(this.state.dokumen === 2){
            return <DokumenTandaTerimaTunjangan 
            resultData={this.state.resultData}
            end_period={this.state.end_period}
            year={this.state.year}
            number={this.state.dokumen}
            namapegawai1={this.state.namapegawai1}
            nip1={this.state.nip1}
            namapegawai2={this.state.namapegawai2}
            nip2={this.state.nip2}
            namapegawai3={this.state.namapegawai3}
            nip3={this.state.nip3}
            tanggal={this.state.tanggal}
            totalPPH={this.state.totalPPH}
        />;
        } else if (this.state.dokumen === 3){
            return <RekapitulasiPPH21 
            resultData={this.state.resultData}
            end_period={this.state.end_period}
            year={this.state.year}
            number={this.state.dokumen}
            namapegawai1={this.state.namapegawai1}
            nip1={this.state.nip1}
            namapegawai2={this.state.namapegawai2}
            nip2={this.state.nip2}
            namapegawai3={this.state.namapegawai3}
            nip3={this.state.nip3}
            tanggal={this.state.tanggal}
            totalPPH={this.state.totalPPH}
        />;
        } else if (this.state.dokumen === 4){
            return <LampiranPMK80 
            resultData={this.state.resultData}
            end_period={this.state.end_period}
            year={this.state.year}
            number={this.state.dokumen}
            namapegawai1={this.state.namapegawai1}
            nip1={this.state.nip1}
            namapegawai2={this.state.namapegawai2}
            nip2={this.state.nip2}
            namapegawai3={this.state.namapegawai3}
            nip3={this.state.nip3}
            tanggal={this.state.tanggal}
        />;
        } else if (this.state.dokumen === 5){
            return <SuratMutlak 
            resultData={this.state.resultData}
            end_period={this.state.end_period}
            year={this.state.year}
            number={this.state.dokumen}
            namapegawai1={this.state.namapegawai1}
            nip1={this.state.nip1}
            namapegawai2={this.state.namapegawai2}
            nip2={this.state.nip2}
            namapegawai3={this.state.namapegawai3}
            nip3={this.state.nip3}
            tanggal={this.state.tanggal}
            suratMutlak={this.state.suratMutlak}
            suratUsulan={this.state.suratUsulan}
            suratKementrian={this.state.suratKementrian}
        />;
        } else if (this.state.dokumen === 6){
            return <SuratUsulan 
            resultData={this.state.resultData}
            end_period={this.state.end_period}
            year={this.state.year}
            number={this.state.dokumen}
            namapegawai1={this.state.namapegawai1}
            nip1={this.state.nip1}
            namapegawai2={this.state.namapegawai2}
            nip2={this.state.nip2}
            namapegawai3={this.state.namapegawai3}
            nip3={this.state.nip3}
            tanggal={this.state.tanggal}
            suratMutlak={this.state.suratMutlak}
            suratUsulan={this.state.suratUsulan}
            suratKementrian={this.state.suratKementrian}

        />;
        }
    }
    render() {
        const { dataList, loading, dokumen, sortBy, orderBy, dictionaries, pageRequest, sizeRequest, totalElement, page } = this.state;
        return (
            <FusePageSimple
                classes={{
                    toolbar: "min-h-48 h-48",
                    rightSidebar: "w-288",
                }}
                content={
                    <div style={{ padding: 52 }}>
                        <Grid container spacing={16}>
                            <Grid item xs={12} className="no-print">
                                <div className="w-full flex flex-col">
                                    <FuseScrollbars className="flex-grow overflow-x-auto">
                                        <Grid style={{ marginTop: 10, marginLeft: 16 }}>
                                            <Formsy>
                                                
                                                <Grid container>
                                                    {/* Komponen untuk pilihan unggah file */}
                                                    <Grid style={{ justifyContent: "center",  marginTop: 8 }} item xs={2} className="pr-24 mb-16">
                                                        Kepala Lapas Salemba
                                                    </Grid>
                                                    <Grid item xs={3} style={{ marginTop: 8 }} className="pr-24 mb-16">
                                                        <TextFieldFormsy
                                                            id="mengetahui1"
                                                            label={"Nama Pegawai"}
                                                            type="text"
                                                            name="namapegawai"
                                                            autoComplete="off"
                                                            variant="outlined"
                                                            placeholder="Nama Pegawai"
                                                            fullWidth
                                                            InputLabelProps={{ shrink: true }}
                                                            value={this.state.namapegawai1}
                                                            onChange={(e) => this.onChangeNamaPegawai1(e.target.value)}
                                                        />
                                                    </Grid>
                                                    <Grid item xs={3} style={{ marginTop: 8 }} className="pr-24 mb-16">
                                                        <TextFieldFormsy
                                                            id="mengetahui2"
                                                            label={"NIP"}
                                                            type="text"
                                                            name="nip"
                                                            autoComplete="off"
                                                            variant="outlined"
                                                            placeholder="NIP"
                                                            fullWidth
                                                            InputLabelProps={{ shrink: true }}
                                                            value={this.state.nip1}
                                                            onChange={(e) => this.onChangeNamaNip1(e.target.value)}
                                                        />
                                                    </Grid>
                                                </Grid>
                                                <Grid container>
                                                    {/* Komponen untuk pilihan unggah file */}
                                                    <Grid style={{ justifyContent: "center",  marginTop: 8 }} item xs={2} className="pr-24 mb-16">
                                                        Pejabat Pembuat Komitmen
                                                    </Grid>
                                                    <Grid item xs={3} style={{ marginTop: 8 }} className="pr-24 mb-16">
                                                        <TextFieldFormsy
                                                            id="mengetahui1"
                                                            label={"Nama Pegawai"}
                                                            type="text"
                                                            name="namapegawai"
                                                            autoComplete="off"
                                                            variant="outlined"
                                                            placeholder="Nama Pegawai"
                                                            fullWidth
                                                            InputLabelProps={{ shrink: true }}
                                                            value={this.state.namapegawai2}
                                                            onChange={(e) => this.onChangeNamaPegawai2(e.target.value)}
                                                        />
                                                    </Grid>
                                                    <Grid item xs={3} style={{ marginTop: 8 }} className="pr-24 mb-16">
                                                        <TextFieldFormsy
                                                            id="mengetahui2"
                                                            label={"NIP"}
                                                            type="text"
                                                            name="nip"
                                                            autoComplete="off"
                                                            variant="outlined"
                                                            placeholder="NIP"
                                                            fullWidth
                                                            InputLabelProps={{ shrink: true }}
                                                            value={this.state.nip2}
                                                            onChange={(e) => this.onChangeNamaNip2(e.target.value)}
                                                        />
                                                    </Grid>
                                                </Grid>
                                                <Grid container>
                                                    {/* Komponen untuk pilihan unggah file */}
                                                    <Grid style={{ justifyContent: "center",  marginTop: 8 }} item xs={2} className="pr-24 mb-16">
                                                        Bendahara Pengeluaran
                                                    </Grid>
                                                    <Grid item xs={3} style={{ marginTop: 8 }} className="pr-24 mb-16">
                                                        <TextFieldFormsy
                                                            id="mengetahui1"
                                                            label={"Nama Pegawai"}
                                                            type="text"
                                                            name="namapegawai"
                                                            autoComplete="off"
                                                            variant="outlined"
                                                            placeholder="Nama Pegawai"
                                                            fullWidth
                                                            InputLabelProps={{ shrink: true }}
                                                            value={this.state.namapegawai3}
                                                            onChange={(e) => this.onChangeNamaPegawai3(e.target.value)}
                                                        />
                                                    </Grid>
                                                    <Grid item xs={3} style={{ marginTop: 8 }} className="pr-24 mb-16">
                                                        <TextFieldFormsy
                                                            id="mengetahui2"
                                                            label={"NIP"}
                                                            type="text"
                                                            name="nip"
                                                            autoComplete="off"
                                                            variant="outlined"
                                                            placeholder="NIP"
                                                            fullWidth
                                                            InputLabelProps={{ shrink: true }}
                                                            value={this.state.nip3}
                                                            onChange={(e) => this.onChangeNamaNip3(e.target.value)}
                                                        />
                                                    </Grid>
                                                </Grid>
                                                <Grid container>
                                                    {/* Komponen untuk pilihan unggah file */}
                                                    <Grid style={{ justifyContent: "center", marginTop: 8 }} item xs={2} className="pr-24 mb-16">
                                                        Tanggal TTD
                                                    </Grid>
                                                    <Grid item xs={6} style={{ marginTop: 8 }} className="pr-24 mb-16">
                                                        <TextFieldFormsy
                                                            id="mengetahui1"
                                                            label={"Tanggal TTD"}
                                                            type="text"
                                                            name="ttgl"
                                                            autoComplete="off"
                                                            variant="outlined"
                                                            placeholder="17 April 2020"
                                                            fullWidth
                                                            InputLabelProps={{ shrink: true }}
                                                            value={this.state.tanggal}
                                                            onChange={(e) => this.onChangeTanggal(e.target.value)}
                                                        />
                                                    </Grid>
                                                    
                                                </Grid>
                                                <Grid container>
                                                    {/* Komponen untuk pilihan unggah file */}
                                                    <Grid style={{ justifyContent: "center", marginTop: 8 }} item xs={2} className="pr-24 mb-16">
                                                        Nomor Surat Pernyataan Tanggung Jawab Mutlak
                                                    </Grid>
                                                    <Grid item xs={6} style={{ marginTop: 8 }} className="pr-24 mb-16">
                                                        <TextFieldFormsy
                                                            id="mengetahui1"
                                                            label={"Nomor Surat"}
                                                            type="text"
                                                            name="nomorsurat"
                                                            autoComplete="off"
                                                            variant="outlined"
                                                            placeholder="W.10.PAS.PAS.3.KU.01.02-013"
                                                            fullWidth
                                                            InputLabelProps={{ shrink: true }}
                                                            value={this.state.suratMutlak}
                                                            onChange={(e) => this.onChangeSuratMutlak(e.target.value)}
                                                        />
                                                    </Grid>
                                                    
                                                </Grid>
                                                <Grid container>
                                                    {/* Komponen untuk pilihan unggah file */}
                                                    <Grid style={{ justifyContent: "center", marginTop: 8 }} item xs={2} className="pr-24 mb-16">
                                                        Nomor Surat Usulan
                                                    </Grid>
                                                    <Grid item xs={6} style={{ marginTop: 8 }} className="pr-24 mb-16">
                                                        <TextFieldFormsy
                                                            id="mengetahui1"
                                                            label={"Nomor Surat"}
                                                            type="text"
                                                            name="nomorsurat2"
                                                            autoComplete="off"
                                                            variant="outlined"
                                                            placeholder="W.10.PAS.PAS.3.KU.01.02-013"
                                                            fullWidth
                                                            InputLabelProps={{ shrink: true }}
                                                            value={this.state.suratUsulan}
                                                            onChange={(e) => this.onChangeSuratUsulan(e.target.value)}
                                                        />
                                                    </Grid>
                                                    
                                                </Grid>
                                                <Grid container>
                                                    {/* Komponen untuk pilihan unggah file */}
                                                    <Grid style={{ justifyContent: "center", marginTop: 8 }} item xs={2} className="pr-24 mb-16">
                                                        Nomor Surat Kementerian dan Tanggal
                                                    </Grid>
                                                    <Grid item xs={6} style={{ marginTop: 8 }} className="pr-24 mb-16">
                                                        <TextFieldFormsy
                                                            id="mengetahui1"
                                                            label={"Nomor Surat"}
                                                            type="text"
                                                            name="nomorsurat3"
                                                            autoComplete="off"
                                                            variant="outlined"
                                                            placeholder="SEK.3.KU.04.01-03 Tanggal 15 Januari 2021"
                                                            fullWidth
                                                            InputLabelProps={{ shrink: true }}
                                                            value={this.state.suratKementrian}
                                                            onChange={(e) => this.onChangeSuratKementrian(e.target.value)}
                                                        />
                                                    </Grid>
                                                    
                                                </Grid>
                                                <Grid container>
                                                    <Grid item xs={2} style={{ justifyContent: "center",  marginTop: 8 }} className="pr-24 mb-16">
                                                        Pilih Periode
                                                    </Grid>

                                                    <Grid item xs={6} className="pr-24 mb-16" style={{ marginTop: 8 }}>
                                                        <SelectFormsy onChange={this.handleChangePeriode} className="w-full" name="period" label="Pilih Periode" value={this.state.period} variant="outlined" autoWidth InputLabelProps={{ shrink: true }} required>
                                                            {this.state.periodList
                                                                ? this.state.periodList.map((n, index) => {
                                                                      return (
                                                                          <MenuItem key={n.uuid} value={n.uuid}>
                                                                              {n.start_period} - {n.end_period} {n.year}
                                                                          </MenuItem>
                                                                      );
                                                                  })
                                                                : ""}
                                                        </SelectFormsy>
                                                    </Grid>
                                                </Grid>

                                                {
                                                    this.state.period !== "" ? (<Grid container>
                                                        <Grid item xs={2} style={{ justifyContent: "center",  marginTop: 8 }} className="pr-24 mb-16">
                                                            Pilih Jenis Dokumen
                                                        </Grid>
    
                                                        <Grid item xs={6} className="pr-24 mb-16" style={{ marginTop: 8 }}>
                                                            <SelectFormsy onChange={this.handleChangeDokumen} className="w-full" name="dokumen" label="Pilih Jenis Dokumen" value={this.state.dokumen} variant="outlined" autoWidth InputLabelProps={{ shrink: true }} required>
                                                                
                                                                <MenuItem key={2} value={2}>
                                                                    Tanda Terima Tunjangan Kinerja Pegawai
                                                                </MenuItem>
                                                                <MenuItem key={3} value={3}>
                                                                    Rekapitulasi PPh Pasal 21 Tunjangan Kinerja Pegawai
                                                                </MenuItem>
                                                                <MenuItem key={4} value={4}>
                                                                    Lampiran A PMK80
                                                                </MenuItem>
                                                                <MenuItem key={5} value={5}>
                                                                    Surat Pernyataan Tanggung Jawab Mutlak
                                                                </MenuItem>
                                                                <MenuItem key={6} value={6}>
                                                                    Surat Usulan Permintaan Pembayaran Tunjungan Kinerja
                                                                </MenuItem>
                                                            </SelectFormsy>
                                                        </Grid>
                                                    </Grid>) : "Pilih Periode untuk memuncul pilihan dokumen"
                                                }
                                                

                                              
                                                
                                                
                                            </Formsy>
                                        </Grid>
                                    </FuseScrollbars>
                                </div>
                            </Grid>

                            <Grid item xs={12}>
                                {
                                    this.returnPage()
                                    
                                }
                               
                            </Grid>
                            

                            <Dialog open={loading} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
                                <DialogTitle id="alert-dialog-title">Sedang proses</DialogTitle>
                                <DialogContent>
                                    <LinearProgress className="w-xs" color="secondary" />
                                </DialogContent>
                            </Dialog>
                        </Grid>
                    </div>
                }
                innerScroll
            />
        );
    }
}

export default Dokumens;
