import React, { Component } from 'react';
import './StyleDokumen.css';




class DokumenTandaTerimaTunjangan extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    formatRupiah = (angka, prefix) => {
        let number_string = angka && angka.toString(),
            split = number_string && number_string.split(","),
            sisa = split && split[0].length % 3,
            rupiah = split && split[0].substr(0, sisa),
            ribuan = split && split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            let separator = sisa ? "." : "";
            rupiah += separator + ribuan.join(".");
        }

        rupiah = split && split[1] !== undefined ? rupiah + "," + split && split[1] : rupiah;
        return prefix === undefined ? rupiah : rupiah ? "" + rupiah : "";
    };

    headerTable = () => {
        return (
            <tr>
                <th style={{ borderWidth: "1px" }}>N0</th>
                <th style={{ borderWidth: "1px", width: 150, minWidth: 150, maxWidth: 150 }}>NAMA/NIP</th>
                <th style={{ borderWidth: "1px", width: 30, minWidth: 30 }}>PANGKAT/GOL</th>
                <th style={{ borderWidth: "1px", width: 200, minWidth: 200, maxWidth: 200 }}>JABATAN</th>
                <th style={{ borderWidth: "1px" }}>GRADE</th>
                <th style={{ borderWidth: "1px" }}>PERINCIAN</th>
                <th style={{ borderWidth: "1px" }}>TUNJ. PPH 21</th>
                <th style={{ borderWidth: "1px" }}>BRUTO</th>
                <th style={{ borderWidth: "1px" }}>POT. PPH 21</th>
                <th style={{ borderWidth: "1px" }}>FAKTOR PENGURANG</th>
                <th style={{ borderWidth: "1px" }}>NETTO</th>
                <th style={{ borderWidth: "1px" }}>TANDA TANGAN</th>
            </tr>
        );
    };

    pagedata = (fromNumber, lastNumber) => {
        var indents = [];
        let confirmation = this.props.resultData.length < lastNumber ? this.props.resultData.length : lastNumber;
        let confirmationTTD = this.props.resultData.length < lastNumber ? true : this.props.resultData.length < 20 ? true : false;
        let jumlahpegawai=0;
        let total_tunkir = 0;
        let total_potongan_pph=0;
        let total_potongan_nilai=0;
        let total_bruto=0;
        let total_netto=0;
        for (let x = fromNumber; x < confirmation; x++) {
            jumlahpegawai += 1;
            let bruto = parseFloat(this.props.resultData[x].tunkir) + parseFloat(this.props.resultData[x].potongan_pph);
            let netto = parseFloat(this.props.resultData[x].tunkir) - parseFloat(this.props.resultData[x].potongan_pph);
            total_tunkir += parseFloat(this.props.resultData[x].tunkir);
            total_potongan_pph += parseFloat(this.props.resultData[x].potongan_pph);
            total_potongan_nilai += parseFloat(this.props.resultData[x].potongan_nilai);
            total_bruto += bruto;
            total_netto += netto;
            indents.push(
                <tr key={x}>
                    <td style={{ borderWidth: "1px" }}>{x + 1}</td>
                    <td style={{ borderWidth: "1px" }}>
                        {this.props.resultData[x].nama}
                        {this.props.resultData[x].nip}
                    </td>
                    <td style={{ borderWidth: "1px" }}>{this.props.resultData[x].pangkat} {this.props.resultData[x].gol}</td>
                    <td style={{ borderWidth: "1px" }}>{this.props.resultData[x].jabatan}</td>
                    <td style={{ borderWidth: "1px" }}>{this.props.resultData[x].grade}</td>
                    <td style={{ borderWidth: "1px" }}>{this.formatRupiah(this.props.resultData[x].tunkir, "") || ""}</td>
                    <td style={{ borderWidth: "1px" }}>{this.formatRupiah(this.props.resultData[x].potongan_pph, "") || ""}</td>
                    <td style={{ borderWidth: "1px" }}>{this.formatRupiah(bruto, "") || " - "}</td>
                    <td style={{ borderWidth: "1px" }}>{this.formatRupiah(this.props.resultData[x].potongan_pph, "") || ""}</td>
                    <td style={{ borderWidth: "1px" }}>{this.formatRupiah(this.props.resultData[x].potongan_nilai, "") || ""}</td>
                    <td style={{ borderWidth: "1px" }}>{this.formatRupiah(netto, "") || ""}</td>
                    <td style={{ borderWidth: "1px" }}></td>
                </tr>
            );
        }
        return (
        <>
            {indents}
            <tr key={1}>
                <td style={{ borderWidth: "1px" }}></td>
                <td style={{ borderWidth: "1px" }}></td>
                <td style={{ borderWidth: "1px" }}>{jumlahpegawai} PEGAWAI</td>
                <td style={{ borderWidth: "1px" }}></td>
                <td style={{ borderWidth: "1px" }}></td>
                <td style={{ borderWidth: "1px" }}>{this.formatRupiah(total_tunkir, "") || ""}</td>
                <td style={{ borderWidth: "1px" }}>{this.formatRupiah(total_potongan_pph, "") || ""} </td>
                <td style={{ borderWidth: "1px" }}>{this.formatRupiah(total_bruto, "") || ""} </td>
                <td style={{ borderWidth: "1px" }}>{this.formatRupiah(total_potongan_pph, "") || ""} </td>
                <td style={{ borderWidth: "1px" }}>{this.formatRupiah(total_potongan_nilai, "") || ""}</td>
                <td style={{ borderWidth: "1px" }}>{this.formatRupiah(total_netto, "") || ""} </td>
                <td style={{ borderWidth: "1px" }}></td>
            </tr>

           
            {
                confirmationTTD && (
                    <>
                     <tr key={1}>
                        <td colSpan={5} style={{ borderWidth: "1px" }}><b>LAPAS KELAS IIA SALEMBA</b></td>
                        <td style={{ borderWidth: "1px" }}>{this.formatRupiah(this.props.totalPPH.JUMLAH_NOMINAL, "") || ""}</td>
                        <td style={{ borderWidth: "1px" }}>{this.formatRupiah(this.props.totalPPH.JUMLAH_PAJAK, "") || ""} </td>
                        <td style={{ borderWidth: "1px" }}>{this.formatRupiah(this.props.totalPPH.JUMLAH_NOMINAL + this.props.totalPPH.JUMLAH_PAJAK, "") || ""} </td>
                        <td style={{ borderWidth: "1px" }}>{this.formatRupiah(this.props.totalPPH.JUMLAH_PAJAK, "") || ""} </td>
                        <td style={{ borderWidth: "1px" }}>{this.formatRupiah(this.props.totalPPH.JUMLAH_POTONGAN, "") || ""}</td>
                        <td style={{ borderWidth: "1px" }}>{this.formatRupiah(this.props.totalPPH.JUMLAH_NOMINAL - this.props.totalPPH.JUMLAH_POTONGAN, "") || ""} </td>
                        <td style={{ borderWidth: "1px" }}></td>
                    </tr>
                    <tr >
                    <td style={{paddingTop:20}} colSpan={6}>
                                Mengetahui/Menyetujui
                                <br/>
                                Kepala Lapas Kelas IIA Salemba
                                <br/>
                                <br/>
                                <br/>
             
                                <b>{this.props.namapegawai1}</b><br/>
                                NIP: {this.props.nip1}
                    </td>
                    <td  style={{paddingTop:20}}  colSpan={6}> 
                                Jakarta, {this.props.tanggal}
                                <br/>
                                Bendahara Pengeluaran
                                <br/>
                                <br/>
                                <br/>
                  
                                <b>{this.props.namapegawai2}</b><br/>
                                NIP: {this.props.nip2}
                    </td> 
             </tr>
             </>
                )
            }
                
            
        </>
        );
    };

    functionTable = () => {
        if(this.props.number === 1) {

        } else if (this.props.number === 2){
            const page1 = this.props.resultData.length > 10 ? true : false;
            const page2 = this.props.resultData.length > 10  ? true : this.props.resultData.length < 20 ? true : false;
            const page3 = this.props.resultData.length > 20  ? true : this.props.resultData.length < 30 ? true : false;
            const page4 = this.props.resultData.length > 30  ? true : this.props.resultData.length < 40 ? true : false;
            const page5 = this.props.resultData.length > 40  ? true : this.props.resultData.length < 50 ? true : false;
            const page6 = this.props.resultData.length > 50  ? true : this.props.resultData.length < 60 ? true : false;
            const page7 = this.props.resultData.length > 60  ? true : this.props.resultData.length < 70 ? true : false;
            const page8 = this.props.resultData.length > 70  ? true : this.props.resultData.length < 80 ? true : false;
            const page9 = this.props.resultData.length > 80  ? true : this.props.resultData.length < 90 ? true : false;
            const page10 = this.props.resultData.length > 90  ? true : this.props.resultData.length < 100 ? true : false;
            const page11 = this.props.resultData.length > 100  ? true : this.props.resultData.length < 110 ? true : false;
            const page12 = this.props.resultData.length > 110  ? true : this.props.resultData.length < 120 ? true : false;
            const page13 = this.props.resultData.length > 120 ? true : this.props.resultData.length < 130 ? true : false;
            const page14 = this.props.resultData.length > 130 ? true : this.props.resultData.length < 140 ? true : false;
            const page15 = this.props.resultData.length > 140 ? true : this.props.resultData.length < 150 ? true : false;
            const page16 = this.props.resultData.length > 150 ? true : this.props.resultData.length < 160 ? true : false;
            const page17 = this.props.resultData.length > 160 ? true : this.props.resultData.length < 170 ? true : false;
            const page18 = this.props.resultData.length > 170 ? true : this.props.resultData.length < 180 ? true : false;
            const page19 = this.props.resultData.length > 180 ? true : this.props.resultData.length < 190 ? true : false;
            const page20 = this.props.resultData.length > 190 ? true : this.props.resultData.length < 200 ? true : false;
            const page21 = this.props.resultData.length > 200 ? true : this.props.resultData.length < 210 ? true : false;
            const page22 = this.props.resultData.length > 210 ? true : this.props.resultData.length < 220 ? true : false;
            const page23 = this.props.resultData.length > 220 ? true : this.props.resultData.length < 230 ? true : false;
            const page24 = this.props.resultData.length > 230 ? true : this.props.resultData.length < 240 ? true : false;
            const page25 = this.props.resultData.length > 240 ? true : this.props.resultData.length < 250 ? true : false;
            const page26 = this.props.resultData.length > 250 ? true : this.props.resultData.length < 260 ? true : false;
            const page27 = this.props.resultData.length > 260 ? true : this.props.resultData.length < 270 ? true : false;
            const page28 = this.props.resultData.length > 270 ? true : this.props.resultData.length < 280 ? true : false;
            const page29 = this.props.resultData.length > 280 ? true : this.props.resultData.length < 290 ? true : false;
            const page30 = this.props.resultData.length > 290 ? true : this.props.resultData.length < 300 ? true : false;

            return (
                <>
                    {page1 === true ? (
                        <div id="section-to-print">
                            <div className="title-print" style={{textAlign:"center", fontWeight:"bold"}}>
                                TANDA TERIMA TUNJANGAN KINERJA PEGAWAI <br/>
                                KANTOR WILAYAH KEMENTRIAN HUKUM DAN HAM DKI JAKARTA <br/>
                                LEMBAGA PEMASYARAKATAN KELAS IIA SALEMBA <br/>
                                BULAN {this.props.end_period.toUpperCase()} TAHUN {this.props.year} <br/>
                            </div>
                        
                            <table className="tableCustom" border="1">
                                {this.headerTable()}
                                {this.pagedata(0, 10)}
                            </table>
                        </div>
                    ) : (
                        ""
                    )}
                    <div style={{ pageBreakBefore: "always" }}>
                        {page2 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(10, 20)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page3 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(20, 30)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page4 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(30, 40)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page5 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(40, 50)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page6 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(50, 60)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page7 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(60, 70)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page8 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(70, 80)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page9 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(80, 90)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page10 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(90, 100)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page11 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(100, 110)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page12 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(110, 120)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page13 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(120, 130)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page14 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(130, 140)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page15 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(140, 150)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page16 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(150, 160)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page17 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(160, 170)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page18 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(170, 180)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page19 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(180, 190)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page20 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(190, 200)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page21 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(200, 210)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page22 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(210, 220)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page23 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(220, 230)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page24 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(230, 240)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page25 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(240, 250)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page26 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(250, 260)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page27 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(260, 270)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page28 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(270, 280)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page29 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(280, 280)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page30 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(290, 300)}
                                </table>
                            </div>
                        )}
                    </div>

                
                </>
            );
        }
        
    };

    render() {
        return (
            <div >
                <style>
                    {`@page {
                        size: A4 landscape
                    }`}
                </style>
                <div className="no-print">
                    Preview:<br/>
                    Tanggal: {this.props.tanggal}<br/>
                    Kepala Lapas: {this.props.namapegawai1}<br/>
                    Kepala Lapas NIP: {this.props.nip1}<br/>
                    Pejabat Pembuat Komitmen: {this.props.namapegawai2}<br/>
                    Pejabat Pembuat Komitmen NIP: {this.props.nip2}<br/>
                    Bendahara Pengeluaran: {this.props.namapegawai3}<br/>
                    Bendahara Pengeluaran NIP: {this.props.nip3}<br/>
                </div>
                
                {this.functionTable()}
            </div>
        );
    }
}


export default DokumenTandaTerimaTunjangan;