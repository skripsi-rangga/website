import React, { Component } from 'react';
import './StyleDokumen.css';

class RekapitulasiPPH21 extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    formatRupiah = (angka, prefix) => {
        let number_string = angka && angka.toString(),
            split = number_string && number_string.split(","),
            sisa = split && split[0].length % 3,
            rupiah = split && split[0].substr(0, sisa),
            ribuan = split && split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            let separator = sisa ? "." : "";
            rupiah += separator + ribuan.join(".");
        }

        rupiah = split && split[1] !== undefined ? rupiah + "," + split && split[1] : rupiah;
        return prefix === undefined ? rupiah : rupiah ? "" + rupiah : "";
    };

    headerTable = () => {
        return (
            <tr>
                <th style={{ borderWidth: "1px", padding: 4 }}>N0</th>
                <th style={{ borderWidth: "1px", padding: 4 }}>NAMA/NIP</th>
                <th style={{ borderWidth: "1px", padding: 4 }}>JABATAN</th>
                <th style={{ borderWidth: "1px", padding: 4}}>NPWP</th>
                <th style={{ borderWidth: "1px", padding: 4 }}>GOL</th>
                <th style={{ borderWidth: "1px", padding: 4 }}>GRADE</th>
                <th style={{ borderWidth: "1px", padding: 4 }}>NOMINAL</th>
                <th style={{ borderWidth: "1px", padding: 4 }}>FAKTOR PENGURANG</th>
                <th style={{ borderWidth: "1px", padding: 4 }}>JUMLAH DITERIMA</th>
                <th style={{ borderWidth: "1px", padding: 4 }}>JUMLAH PAJAK</th>
            </tr>
        );
    };

    pagedata = (fromNumber, lastNumber) => {
        var indents = [];
        let confirmation = this.props.resultData.length < lastNumber ? this.props.resultData.length : lastNumber;
        let confirmationTTD = this.props.resultData.length < lastNumber ? true : false;
        let jumlahpegawai=0;
        let total_tunkir =0;
        let total_potongan_pph =0;
        let total_potongan_nilai =0;
        let total_diterima=0;
        let total_bruto =0;
        let total_netto =0;

        for (let x = fromNumber; x < confirmation; x++) {
            jumlahpegawai += 1;
            let bruto = parseFloat(this.props.resultData[x].tunkir) + parseFloat(this.props.resultData[x].potongan_pph);
            let netto = parseFloat(this.props.resultData[x].tunkir) - parseFloat(this.props.resultData[x].potongan_pph);
            total_tunkir += parseFloat(this.props.resultData[x].tunkir);
            total_potongan_pph += parseFloat(this.props.resultData[x].potongan_pph);
            total_potongan_nilai += parseFloat(this.props.resultData[x].potongan_nilai);
            total_diterima += parseFloat(this.props.resultData[x].diterima);
            total_bruto += bruto;
            total_netto += netto;
            indents.push(
                <tr key={x}>
                    <td style={{ borderWidth: "1px" }}>{x + 1}</td>
                    <td style={{ borderWidth: "1px" }}>
                        {this.props.resultData[x].nama}
                        {this.props.resultData[x].nip}
                    </td>
                    <td style={{ borderWidth: "1px" }}>{this.props.resultData[x].jabatan}</td>
                    <td style={{ borderWidth: "1px" }}>{this.props.resultData[x].npwp}</td>
                    <td style={{ borderWidth: "1px" }}>{this.props.resultData[x].gol}</td>
                    <td style={{ borderWidth: "1px" }}>{this.props.resultData[x].grade}</td>
                    <td style={{ borderWidth: "1px" }}>{this.formatRupiah(this.props.resultData[x].tunkir, "") || ""}</td>
                    <td style={{ borderWidth: "1px" }}>{this.formatRupiah(this.props.resultData[x].potongan_nilai, "") || ""}</td>
                    <td style={{ borderWidth: "1px" }}>{this.formatRupiah(this.props.resultData[x].diterima, "") || ""}</td>
                    <td style={{ borderWidth: "1px" }}>{this.formatRupiah(this.props.resultData[x].potongan_pph, "") || ""}</td>
                </tr>
            );
        }
        console.log("total_potongan_pph :", total_potongan_pph)
        return (
        <>
            {indents}

            {
                confirmationTTD && (
                    <>
                        <tr key={999}>
                            <td style={{ borderWidth: "1px" }}></td>
                            <td style={{ borderWidth: "1px" }}>
                              
                            </td>
                            <td style={{ borderWidth: "1px" }}></td>
                            <td style={{ borderWidth: "1px" }}></td>
                            <td style={{ borderWidth: "1px" }}></td>
                            <td style={{ borderWidth: "1px" }}></td>
                            <td style={{ borderWidth: "1px" }}>{this.formatRupiah(this.props.totalPPH.JUMLAH_NOMINAL, "") || ""}</td>
                            <td style={{ borderWidth: "1px" }}>{this.formatRupiah(this.props.totalPPH.JUMLAH_POTONGAN, "") || ""}</td>
                            <td style={{ borderWidth: "1px" }}>{this.formatRupiah(this.props.totalPPH.JUMLAH_DITERIMA, "") || ""}</td>
                            <td style={{ borderWidth: "1px" }}>{this.formatRupiah(this.props.totalPPH.JUMLAH_PAJAK, "") || ""}</td>
                        </tr>
                        <tr >
                            <td style={{paddingTop:20}} colSpan={6}>
                                        {/* Mengetahui/Menyetujui
                                        <br/>
                                        Kepala Lapas Kelas IIA Salemba
                                        <br/>
                                        <br/>
                                        <br/>
                    
                                        <b>{this.props.namapegawai1}</b><br/>
                                        NIP: {this.props.nip1} */}
                            </td>
                            <td  style={{paddingTop:20}}  colSpan={6}> 
                                        Jakarta, {this.props.tanggal}
                                        <br/>
                                        Kepala Lapas Kelas IIA Salemba
                                        <br/>
                                        <br/>
                                        <br/>
                    
                                        <b>{this.props.namapegawai1}</b><br/>
                                        NIP: {this.props.nip1}
                            </td> 
                        </tr>
                    </>
                )
            }
                
            
        </>
        );
    };

    functionTable = () => {
        if(this.props.number === 1) {

        } else if (this.props.number === 2){
            
        } else if (this.props.number === 3){
            const page1 = this.props.resultData.length > 15 ? true : false;
            const page2 = this.props.resultData.length > 15 ? true : this.props.resultData.length < 30 ? true : false;
            const page3 = this.props.resultData.length > 30 ? true : this.props.resultData.length < 45 ? true : false;
            const page4 = this.props.resultData.length > 45 ? true : this.props.resultData.length < 60 ? true : false;
            const page5 = this.props.resultData.length > 60 ? true :  this.props.resultData.length < 75 ? true : false;
            const page6 = this.props.resultData.length > 75 ? true :  this.props.resultData.length < 90 ? true : false;
            const page7 = this.props.resultData.length > 90 ? true : this.props.resultData.length < 105 ? true : false;
            const page8 = this.props.resultData.length > 105 ? true :  this.props.resultData.length < 120 ? true : false;
            const page9 = this.props.resultData.length > 120 ? true :  this.props.resultData.length < 135 ? true : false;
            const page10 = this.props.resultData.length > 135 ? true :  this.props.resultData.length < 150 ? true : false;
            const page11 = this.props.resultData.length > 150 ? true :  this.props.resultData.length < 165 ? true : false;
            const page12 = this.props.resultData.length > 165 ? true :  this.props.resultData.length < 180 ? true : false;
            const page13 = this.props.resultData.length > 180 ? true :  this.props.resultData.length < 195 ? true : false;
            const page14 = this.props.resultData.length > 195 ? true :  this.props.resultData.length < 205 ? true : false;
            const page15 = this.props.resultData.length > 205 ? true :  this.props.resultData.length < 220 ? true : false;
            const page16 = this.props.resultData.length > 220 ? true :  this.props.resultData.length < 235 ? true : false;
            const page17 = this.props.resultData.length > 235 ? true :  this.props.resultData.length < 250 ? true : false;
            const page18 = this.props.resultData.length > 250 ? true :  this.props.resultData.length < 265 ? true : false;
            const page19 = this.props.resultData.length > 265 ? true :  this.props.resultData.length < 280 ? true : false;
            const page20 = this.props.resultData.length > 280 ? true :  this.props.resultData.length < 295 ? true : false;
            const page21 = this.props.resultData.length > 295 ? true :  this.props.resultData.length < 310 ? true : false;
            const page22 = this.props.resultData.length > 310 ? true :  this.props.resultData.length < 325 ? true : false;
          
            return (
                <>
                    {page1 === true ? (
                        <div id="section-to-print">
                            <div className="title-print" style={{textAlign:"center", fontWeight:"bold"}}>
                                REKAPITULASI PPh PASAL 21 TUNJANGAN KINERJA PEGAWAI PADA <br/>
                                LEMBAGA PEMASYARAKATAN KELAS IIA SALEMBA <br/>
                                BULAN {this.props.end_period.toUpperCase()} TAHUN {this.props.year} <br/>
                            </div>
                        
                            <table className="tableCustom" border="1">
                                {this.headerTable()}
                                {this.pagedata(0, 15)}
                            </table>
                        </div>
                    ) : (
                        ""
                    )}
                    <div style={{ pageBreakBefore: "always" }}>
                        {page2 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(15, 30)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page3 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(30, 45)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page4 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(45, 60)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page5 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(60, 75)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page6 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(75, 90)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page7 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(90, 105)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page8 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(105, 120)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page9 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(120, 135)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page10 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(135, 150)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page11 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(150, 165)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page12 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(165, 180)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page13 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(180, 195)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page14 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(195, 205)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page15 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(205, 220)}
                                </table>
                            </div>
                        )}
                    </div>


                    <div style={{ pageBreakBefore: "always" }}>
                            {page16 && (
                                <div id="section-to-print">
                                    <table className="tableCustom" border="1">
                                        {this.headerTable()}
                                        {this.pagedata(220, 235)}
                                    </table>
                                </div>
                            )}
                        </div>
                    
                    <div style={{ pageBreakBefore: "always" }}>
                        {page17 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(235, 250)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page18 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(250, 265)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page19 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(265, 280)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page20 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(280, 295)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page21 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(295, 310)}
                                </table>
                            </div>
                        )}
                    </div>
                    <div style={{ pageBreakBefore: "always" }}>
                        {page22 && (
                            <div id="section-to-print">
                                <table className="tableCustom" border="1">
                                    {this.headerTable()}
                                    {this.pagedata(310, 325)}
                                </table>
                            </div>
                        )}
                    </div>
                    
                </>
            );
        }
        
    };

    render() {
        return (
            <div>
                <style>
                    {`@page {
                        size: A4 portrait
                    }`}
                </style>
                <div className="no-print">
                    Preview:<br/>
                    Tanggal: {this.props.tanggal}<br/>
                    Kepala Lapas: {this.props.namapegawai1}<br/>
                    Kepala Lapas NIP: {this.props.nip1}<br/>
                    Pejabat Pembuat Komitmen: {this.props.namapegawai2}<br/>
                    Pejabat Pembuat Komitmen NIP: {this.props.nip2}<br/>
                    Bendahara Pengeluaran: {this.props.namapegawai3}<br/>
                    Bendahara Pengeluaran NIP: {this.props.nip3}<br/>
                </div>
                
                {this.functionTable()}
            </div>
        );
    }
}


export default RekapitulasiPPH21;