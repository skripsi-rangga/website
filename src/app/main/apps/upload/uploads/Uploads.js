/*eslint-disable no-unused-vars*/
import React, { Component } from "react";
import { Button, Typography, MenuItem, Paper, Grid, Icon } from "@material-ui/core";
import { Link } from "react-router-dom";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import api from "@api";
import Formsy from "formsy-react";
import { TextFieldFormsy, SelectFormsy, FuseChipSelect, FusePageSimple, FuseAnimate, FuseScrollbars } from "@fuse";
import * as XLSX from "xlsx";
import moment from "moment";
import _ from "@lodash";
import { BackLink, MPDialog, CalculationForm } from "../../../../../@component";
import { Dialog, DialogTitle, DialogContent, LinearProgress } from "@material-ui/core";
import { TablePagination } from "@material-ui/core";

class Uploads extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formObject: [],
            start_period: "Januari",
            end_period: "Januari",
            data: [],
            isLoading:false,
            msg: "",
            msgState: false,
            msgStateFailed: false,
        };
    }

    funcStartPeriod = (event) => {
        this.setState({
            start_period: event.target.value,
        });
    };

    funcEndPeriod = (event) => {
        this.setState({
            end_period: event.target.value,
        });
    };

    onSubmit = (model) => {
        const { data } = this.state;
        const uuid_period = this.create_UUID();

        // ini untuk mengubah kondisi isLoading menjadi true, shingga akan muncup POP UP Dialog untuk loading
        this.setState({
            isLoading:true,
        })
        let end_period;
        let start_period;
        if(model.start_period === "Januari"){
            start_period = "Desember";
            end_period = "Januari";
        } else if (model.start_period === "Februari"){
            start_period = "Januari";
            end_period = "Februari";
        } else if (model.start_period === "Maret"){
            start_period = "Februari";
            end_period = "Maret";
        } else if (model.start_period === "April"){
            start_period = "Maret";
            end_period = "April";
        } else if (model.start_period === "Mei"){
            start_period = "April";
            end_period = "Mei";
        } else if (model.start_period === "Juni"){
            start_period = "Mei";
            end_period = "Juni";
        } else if (model.start_period === "Juli"){
            start_period = "Juni";
            end_period = "Juli";
        } else if (model.start_period === "Agustus"){
            start_period = "Juli";
            end_period = "Agustus";
        } else if (model.start_period === "September"){
            start_period = "Agustus";
            end_period = "September";
        } else if (model.start_period === "Oktober"){
            start_period = "September";
            end_period = "Oktober";
        } else if (model.start_period === "November"){
            start_period = "Oktober";
            end_period = "November";
        } else if (model.start_period === "Desember"){
            start_period = "November";
            end_period = "Desember";
        } 
        const test = {
            uuid_period,
            start_period,
            end_period,
            year: model.tahun,
            data,
        };

        console.log("ini hasil: ", test)
        api.post("api/tunkir/post", {
            uuid_period,
            start_period,
            end_period,
            year: model.tahun,
            data,
        })
            .then((res) => {
                if (res.data.success) {
                    this.setState({
                        isLoading: false,
                        msgState: true,
                        msg: "Sukses menambah data",
                    });
                } else {
                    this.setState({
                        isLoading: false,
                        msgStateFailed: true,
                        msg: "Gagal Menambahkan data, pastikan mengisi seluruh data dengan benar",
                    });
                }
            })
            .catch((err) => {
                this.setState({
                    isLoading: false,
                    isLoading: false,
                    msgState: true,
                    msgStateFailed: true,
                    msg: "Add data failed",
                });
            });
    };

    uploadData = () => {
        const fileUpload = document.getElementById("fileUpload");
        const regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
        if (regex.test(fileUpload.value.toLowerCase())) {
            let fileName = fileUpload.files[0].name;
            if (typeof FileReader !== "undefined") {
                const reader = new FileReader();
                if (reader.readAsBinaryString) {
                    reader.onload = (e) => {
                        this.prosesExcel(reader.result);
                    };
                    reader.readAsBinaryString(fileUpload.files[0]);
                }
            } else {
                console.log("This browser does not support HTML5.");
            }
        } else {
            console.log("Please upload a valid Excel file.");
        }
    };


    formatRupiah=(angka, prefix)=>{
        let number_string = angka && angka.toString(),
        split   		= number_string && number_string.split(','),
        sisa     		= split && split[0].length % 3,
        rupiah     		= split && split[0].substr(0, sisa),
        ribuan     		= split && split[0].substr(sisa).match(/\d{3}/gi);
       
        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
          let separator = sisa ? '.' : '';
          rupiah += separator + ribuan.join('.');
        }
       
        rupiah = split && split[1] !== undefined ? rupiah + ',' + split && split[1] : rupiah;
        return prefix === undefined ? rupiah : (rupiah ? '' + rupiah : '');
      }
      

    prosesExcel = (data) => {
        const workbook = XLSX.read(data, { type: "binary" });
        const firstSheet = workbook.SheetNames[0];
        const excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
        for (var x in excelRows) {
            // ubah bagian nama untuk menghilangkan nip
            const name = excelRows[x].nama; // masukkan ke variabel nama untuk kolom nama
            const names = name.substring(name.indexOf("\n") + 1);  // sebelum breakspace  hapus data jadi hasilnya nip
            const remove_space = names.replace(/ /g,'')
            excelRows[x].nama = remove_space;
            excelRows[x].fullname = name;

            // ubah bagian nama untuk nama
            const fullname = excelRows[x].fullname;
            const fullname2 = fullname.replace(new RegExp("[0-9]", "g"), "")
            excelRows[x].onlyname = fullname2;

            // ubah diterima to int, menghapus tanda koma
            const diterima = excelRows[x].diterima;
            const diterima_after = parseFloat(diterima.replace(/,/g, ""));
            excelRows[x].diterima = diterima_after;

            // ubah tunkir to int, menghapus tanda koma dan tanda Rp.
            const tunkir = excelRows[x].tunkir;
            const tunkir_remove_koma = tunkir.replace(/,/g, "");
            const tunkir_remove = parseFloat(tunkir_remove_koma.replace("Rp.", ""));
            excelRows[x].tunkir = tunkir_remove;

            // ubah potongan_persen to int, menghapus tanda persentase
            const potongan_persen = excelRows[x].potongan_persen;
            const potongan_persen_remove_persen = potongan_persen.replace("%", "");
            excelRows[x].potongan_persen = parseFloat(potongan_persen_remove_persen);

            // ubah potongan_nilai to int, menghapus tanda koma
            const potongan_nilai = excelRows[x].potongan_nilai;
            const potongan_nilai_remove = potongan_nilai.replace(/,/g, "");
            excelRows[x].potongan_nilai = parseFloat(potongan_nilai_remove);

            // ubah potongan_pph to int, menghapus tanda koma
            const potongan_pph = excelRows[x].potongan_pph;
            const potongan_pph_remove = potongan_pph.replace(/,/g, "");
            excelRows[x].potongan_pph = parseFloat(potongan_pph_remove);
        }
        this.setState({
            data: excelRows,
        });
    };

    create_UUID = () => {
        var dt = new Date().getTime();
        var uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
            var r = (dt + Math.random() * 16) % 16 | 0;
            dt = Math.floor(dt / 16);
            return (c == "x" ? r : (r & 0x3) | 0x8).toString(16);
        });
        return uuid;
    };

    handleClose = () => {
        const { history } = this.props;
        history.push("/apps/list");
        this.setState({
            msgState: false,
        });
    };

    handleCloseFailed = () => {
        this.setState({
            msgStateFailed: false,
        });
    };

    onChangeYears(tahun) {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
        this.timeout = setTimeout(() => {
            this.setState({ tahun });
        }, 500);
    }

    // Ubah halaman di render()
    render() {
        const { isLoading, formObject, msgState, msgStateFailed,msg } = this.state;
        return (
            <FusePageSimple
                classes={{
                    toolbar: "min-h-48 h-48",
                    rightSidebar: "w-288",
                }}
                header={
                    <div className="flex flex-col justify-between flex-1 px-24 pt-24">
                        <div className="flex justify-between items-start">
                            <div className="flex items-center max-w-full">
                                <div className="flex flex-col min-w-0">
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography role="button" className="text-16 sm:text-20 truncate">
                                            Upload Tunjangan Kinerja
                                        </Typography>
                                    </FuseAnimate>
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography variant="caption">Upload tunjangan kinerja via excel</Typography>
                                    </FuseAnimate>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                content={
                    <div style={{padding:52}}>


                        {/* ini pop up untuk pemberitahuan jika berhasil */}
                         <MPDialog
                            open={msgState}
                            onClose={() => this.handleClose()}
                            title={"Pemberitahuan"}
                            content={msg}
                            colorTitle="green"
                            agreeText="Close"
                            onAgree={() => this.handleClose()}
                            fullWidth={true}
                        />

                        {/* ini pop up untuk pemberitahuan jika gagal */}
                        <MPDialog
                            open={msgStateFailed}
                            onClose={() => this.handleCloseFailed()}
                            title={"Pemberitahuan"}
                            content={msg}
                            colorTitle="red"
                            agreeText="Close"
                            onAgree={() => this.handleCloseFailed()}
                            fullWidth={true}
                        />
                        
                        <Grid container spacing={16}>
                            <Grid item xs={12}>
                                <div className="w-full flex flex-col">
                                    <FuseScrollbars className="flex-grow overflow-x-auto">
                                        <Grid>
                                            <Formsy
                                                onValid={this.onValid}
                                                onInvalid={this.onInvalid}
                                                onSubmit={this.onSubmit}
                                                onChange={(e) => {
                                                    this.setState({ formObject: e });
                                                }}
                                                ref={(form) => (this.form = form)}
                                                className="justify-center mt-16 ml-16"
                                            >
                                                <Grid container >

                                                    {/* Komponen untuk pilihan periode */}
                                                    <Grid item xs={2} style={{ justifyContent: "center", lineHeight: "56px", marginTop:16,  }} className="pr-24 mb-16">
                                                        Pilih Periode
                                                    </Grid>

                                                    <Grid item xs={4} className="pr-24 mb-16" style={{marginTop:16}}>
                                                        <SelectFormsy
                                                            onChange={this.funcStartPeriod}
                                                            className="w-full"
                                                            name="start_period"
                                                            label={`Pilih Periode Awal`}
                                                            value={this.state.start_period}
                                                            variant="outlined"
                                                            autoWidth
                                                            InputLabelProps={{ shrink: true }}
                                                            required
                                                        >
                                                            <MenuItem key="Januari" value="Januari">
                                                                Desember - Januari
                                                            </MenuItem>
                                                            <MenuItem key="Februari" value="Februari">
                                                                Januari - Februari
                                                            </MenuItem>
                                                            <MenuItem key="Maret" value="Maret">
                                                                Februari - Maret
                                                            </MenuItem>
                                                            <MenuItem key="April" value="April">
                                                                Maret - April
                                                            </MenuItem>
                                                            <MenuItem key="Mei" value="Mei">
                                                                April - Mei
                                                            </MenuItem>
                                                            <MenuItem key="Juni" value="Juni">
                                                                Mei - Juni
                                                            </MenuItem>
                                                            <MenuItem key="Juli" value="Juli">
                                                                Juni - Juli
                                                            </MenuItem>
                                                            <MenuItem key="Agustus" value="Agustus">
                                                                Juli - Agustus
                                                            </MenuItem>
                                                            <MenuItem key="September" value="September">
                                                                Agustus - September
                                                            </MenuItem>
                                                            <MenuItem key="Oktober" value="Oktober">
                                                                September - Oktober
                                                            </MenuItem>
                                                            <MenuItem key="November" value="November">
                                                                Oktober - November
                                                            </MenuItem>
                                                            <MenuItem key="Desember" value="Desember">
                                                                November - Desember
                                                            </MenuItem>
                                                        </SelectFormsy>
                                                    </Grid>
                                                    
                                                    <Grid item xs={6} className="pr-24 mb-16" style={{marginTop:16}}>
                                                        <TextFieldFormsy
                                                            // style={{ color: "red" }}
                                                            label={"Tahun"}
                                                            type="text"
                                                            name="tahun"
                                                            autoComplete="off"
                                                            variant="outlined"
                                                            placeholder="Tahun"
                                                            fullWidth
                                                            value={this.state.tahun}
                                                            InputLabelProps={{ shrink: true }}
                                                            onChange={(e) => this.onChangeYears(e.target.value)}
                                                            
                                                        />
                                                    </Grid>

                                                    {/* Komponen untuk pilihan unggah file */}
                                                    <Grid style={{ justifyContent: "center", lineHeight: "56px", marginTop:20 }} item xs={2} className="pr-24 mb-16">
                                                        Unggah file dengan format Excel
                                                    </Grid>
                                                    <Grid item xs={10} style={{marginTop:20}} className="pr-24 mb-16">
                                                      

                                                        <TextFieldFormsy
                                                            id="fileUpload"
                                                            label={"Upload File"}
                                                            type="file"
                                                            name="inputdata"
                                                            autoComplete="off"
                                                            variant="outlined"
                                                            placeholder="Upload File"
                                                            fullWidth
                                                            InputLabelProps={{ shrink: true }}
                                                            onChange={this.uploadData}
                                                        />
                                                    </Grid>



                                                    

                                                    
                                                </Grid>
                                                <Grid container>
                                                    <Grid item xs={10} className="pr-24 mb-16">
                                                        <Button
                                                            type="submit"
                                                            variant="contained"
                                                            color="primary"
                                                            value="legacy"
                                                            disabled={isLoading}
                                                            size="large"
                                                        >
                                                            Simpan
                                                        </Button>
                                                    </Grid>
                                                </Grid>
                                            </Formsy>
                                        </Grid>
                                    </FuseScrollbars>
                                </div>
                                <div className="w-full flex flex-col">
                                <FuseScrollbars className="flex-grow overflow-x-auto">
                                <Table   className="min-w-xl" aria-labelledby="tableTitle">
                                    <TableHead style={{backgroundColor:"#0064D2", color:"#FFFFFF"}}>
                                        <TableRow>
                                            <TableCell style={{color:"#FFFFFF"}} rowSpan={2}>No</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}} rowSpan={2}>Nama</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}} rowSpan={2}>Jabatan</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}} rowSpan={2}>Grade</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}} rowSpan={2}>Status</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}} rowSpan={2}>Tunkir</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}} rowSpan={2}>Hari Kerja</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}} rowSpan={2}>Hadir</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}} rowSpan={2}>Aplha</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}} rowSpan={2}>DL</TableCell>
                                            <TableCell style={{color:"#FFFFFF", textAlign:"center"}} colSpan={2}>izin</TableCell>
                                            <TableCell style={{color:"#FFFFFF", textAlign:"center"}} colSpan={6}>Cuti</TableCell>
                                            <TableCell style={{color:"#FFFFFF", textAlign:"center"}} colSpan={4}>Telat</TableCell>
                                            <TableCell style={{color:"#FFFFFF", textAlign:"center"}} colSpan={4}>Cepat Pulang</TableCell>
                                            <TableCell style={{color:"#FFFFFF", textAlign:"center"}} colSpan={3}>Jurnal</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}} rowSpan={2}>Ganti</TableCell>
                                            <TableCell style={{color:"#FFFFFF", textAlign:"center"}} colSpan={2}>Potongan</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}} rowSpan={2}>Diterima</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}} rowSpan={2}>Potongan PPH</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell style={{color:"#FFFFFF", borderLeft:"1px solid #FFFFFF"}}>Telat</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}}>Pulang</TableCell>
                                            <TableCell style={{color:"#FFFFFF", borderLeft:"1px solid #FFFFFF"}}>Tahun</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}}>Sakit</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}}>Hamil</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}}>Penting</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}}>Besar</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}}>Lainnya</TableCell>
                                            <TableCell style={{color:"#FFFFFF", borderLeft:"1px solid #FFFFFF"}}>30</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}}>60</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}}>90</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}}>90+</TableCell>
                                            <TableCell style={{color:"#FFFFFF", borderLeft:"1px solid #FFFFFF"}}>30</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}}>60</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}}>90</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}}>90+</TableCell>
                                            <TableCell style={{color:"#FFFFFF", borderLeft:"1px solid #FFFFFF"}}>2</TableCell>
                                            <TableCell style={{color:"#FFFFFF"}}>1</TableCell>
                                            <TableCell style={{color:"#FFFFFF",borderRight:"1px solid #FFFFFF"}}>0</TableCell>
                                           
                                            <TableCell style={{color:"#FFFFFF", borderLeft:"1px solid #FFFFFF"}}>Persentase</TableCell>
                                            <TableCell style={{color:"#FFFFFF", borderRight:"1px solid #FFFFFF"}}>Nilai</TableCell>
                                            
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {this.state.data
                                            ? this.state.data.map((n, index) => {
                                                    return (
                                                        <TableRow className="h-64 cursor-pointer" hover key={n.no}>
                                                            <TableCell>{n.no}</TableCell>
                                                            <TableCell>{n.fullname}</TableCell>
                                                            <TableCell>{n.jabatan}</TableCell>
                                                            <TableCell>{n.grade}</TableCell>
                                                            <TableCell>{n.status}</TableCell>
                                                            <TableCell> {this.formatRupiah(n.tunkir, '') || ' - '}</TableCell>
                                                            <TableCell>{n.harikerja}</TableCell>
                                                            <TableCell>{n.hadir}</TableCell>
                                                            <TableCell>{n.aplha}</TableCell>
                                                            <TableCell>{n.dl}</TableCell>
                                                            <TableCell>{n.izin_telat}</TableCell>
                                                            <TableCell>{n.izin_pulang}</TableCell>
                                                            <TableCell>{n.cuti_tahun}</TableCell>
                                                            <TableCell>{n.cuti_sakit}</TableCell>
                                                            <TableCell>{n.cuti_hamil}</TableCell>
                                                            <TableCell>{n.cuti_penting}</TableCell>
                                                            <TableCell>{n.cuti_besar}</TableCell>
                                                            <TableCell>{n.cuti_lainnya}</TableCell>
                                                            <TableCell>{n.telat_30}</TableCell>
                                                            <TableCell>{n.telat_60}</TableCell>
                                                            <TableCell>{n.telat_90a}</TableCell>
                                                            <TableCell>{n.telat_90b}</TableCell>
                                                            <TableCell>{n.cepat_pulang_30}</TableCell>
                                                            <TableCell>{n.cepat_pulang_60}</TableCell>
                                                            <TableCell>{n.cepat_pulang_90a}</TableCell>
                                                            <TableCell>{n.cepat_pulang_90b}</TableCell>
                                                            <TableCell>{n.jurnal_2}</TableCell>
                                                            <TableCell>{n.jurnal_1}</TableCell>
                                                            <TableCell>{n.jurnal_0}</TableCell>
                                                            <TableCell>{n.ganti}</TableCell>
                                                            <TableCell>{n.potongan_persen}</TableCell>
                                                            <TableCell>{n.potongan_nilai}</TableCell>
                                                            <TableCell>{n.diterima}</TableCell>
                                                            <TableCell>{n.potongan_pph}</TableCell>
                                                            
                                                        </TableRow>
                                                    );
                                                })
                                            : ""}
                                    </TableBody>
                                </Table>
                                </FuseScrollbars>
                                </div>
                            </Grid>
                            <Dialog open={isLoading} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
                                <DialogTitle id="alert-dialog-title">Sedang proses</DialogTitle>
                                <DialogContent>
                                    <LinearProgress className="w-xs" color="secondary" />
                                </DialogContent>
                            </Dialog>
                        </Grid>
                    </div>
                }
                innerScroll
            />
        );
    }
}

export default Uploads;
