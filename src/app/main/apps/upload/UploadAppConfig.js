import React from 'react';
import {FuseLoadable} from '@fuse';
import {Redirect} from 'react-router-dom';

export const UploadAppConfig = {
    settings: {
        layout: {}
    },
    routes  : [
        {
            path     : '/apps/upload',
            component: FuseLoadable({
                loader: () => import('./uploads/Uploads')
            })
        },
        {
            path     : '/apps/upload',
            component: () => <Redirect to="/apps/upload/uploads"/>
        }
    ]
};