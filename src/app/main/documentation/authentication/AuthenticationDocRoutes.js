import {FuseLoadable} from '@fuse';

export const AuthenticationDocRoutes = [
    {
        path     : '/documentation/authentication/jwt',
        component: FuseLoadable({
            loader: () => import('./jwt/jwtAuthDoc')
        })
    },
];
