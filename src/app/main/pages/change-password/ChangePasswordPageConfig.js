import {FuseLoadable} from '@fuse';

export const ChangePasswordPageConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path     : '/pages/change-password',
            component: FuseLoadable({
                loader: () => import('./ChangePasswordPage')
            })
        }
    ]
};
