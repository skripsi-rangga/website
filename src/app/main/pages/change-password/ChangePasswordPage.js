import React, { Component } from "react";
import {
  withStyles,
  Button,
  Card,
  CardContent,
  Typography,
} from "@material-ui/core";
import { darken } from "@material-ui/core/styles/colorManipulator";
import { FuseAnimate } from "@fuse";
import classNames from "classnames";
import Formsy from "formsy-react";
import { TextFieldFormsy } from "@fuse";
import api from "@api";
import MPDialog from '../../../../@component/MPDialog';

const styles = theme => ({
  root: {
    background:
      "radial-gradient(" +
      darken(theme.palette.primary.dark, 0.5) +
      " 0%, " +
      theme.palette.primary.dark +
      " 80%)",
    color: theme.palette.primary.contrastText
  }
});

class ChangePasswordPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      success: false,
      fail: false,
      msg: "",
      canSubmit: false
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  disableButton = () => {
    this.setState({ canSubmit: false });
  };

  enableButton = () => {
    this.setState({ canSubmit: true });
  };

  onSubmit = async model => {
    api
      .post(
        `${process.env.REACT_APP_API_HOST}${process.env.REACT_APP_API_CHANGE_PASSWORD}`,
        model
      )
      .then(response => {
        if (response.status === 201 || response.status === 200) {
					this.setState({ 
						success: true,
						msg: "Anda berhasil merubah password"
					});
        } else {
          this.setState({
            fail: true,
            msg: "Pastikan Anda mengisi data dengan benar"
          });
        }
      });
  };

	handleCloseSuccess = () => {
    this.setState({
      fail: false,
      success: false,
    });
	};
	
  render() {
    const { classes } = this.props;
		const { success, fail, canSubmit, value, msg } = this.state;
		const userData = localStorage.getItem('user_data');
		const obj = JSON.parse(userData);
		const { data } = obj;
		const {displayName} =  data;
    return (
      <div
        className={classNames(
          classes.root,
          "flex flex-col flex-auto flex-no-shrink items-center justify-center p-32"
        )}
      >
        <div className="flex flex-col items-center justify-center w-full">
          <FuseAnimate animation="transition.expandIn">
            <Card className="w-full max-w-384">
              <CardContent className="flex flex-col items-center justify-center p-32 text-center">
                <img
                  className="w-128 m-32"
                  src="assets/images/logos/fuse.svg"
                  alt="logo"
                />

                <Typography variant="subtitle1" className="mb-16">
                  Change Password
                </Typography>

                <Typography color="textSecondary" className="max-w-288">
                  In order to <b>protect your account</b>, <br />
                  make sure your password:
                </Typography>

                <Typography className="font-bold my-32 w-full">
                  Is longer than 6 characters.
                </Typography>

                <Formsy
                  onValidSubmit={this.onSubmit}
                  onValid={this.enableButton}
                  onInvalid={this.disableButton}
                  ref={form => (this.form = form)}
                  className="flex flex-col justify-center w-full"
                >
                  <TextFieldFormsy
                    className="mb-16"
                    label="Old Password"
                    type="password"
                    name="oldPassword"
                    autoComplete="off"
                    validations={{
                      minLength: 6,
                      maxLength: 30
                    }}
                    validationErrors={{
                      minLength: `Min character length is 6`,
                      maxLength: `Max character length is 30`
                    }}
                    variant="outlined"
                    required
                  />

                  <TextFieldFormsy
                    className="mb-16"
                    label="New Password"
                    type="password"
                    name="password"
                    autoComplete="off"
                    onChange={this.handleChange}
                    validations={{
                      minLength: 6
                    }}
                    validationErrors={{
                      minLength: "Min character length is 6"
                    }}
                    variant="outlined"
                    required
                  />

                  <TextFieldFormsy
                    className="mb-16"
                    label="New Password Confirmation"
                    type="password"
                    name="confirmPassword"
                    autoComplete="off"
                    validationError="Your password didn't match"
                    validations={{
                      minLength: 6,
                      equals: value
                    }}
                    validationErrors={{
                      minLength: "Min character length is 6"
                    }}
                    variant="outlined"
                    required
                  />

									<TextFieldFormsy
                    type="hidden"
										name="username"
										value={displayName}
                  />

                  <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    value="legacy"
                    disabled={!canSubmit}
                  >
                    Submit
                  </Button>
                </Formsy>

								<MPDialog
									open={success}
									title="Berhasil"
									content={msg}
									styleOnAgree={{ color: '#0064D2' }}
									onAgree={() => this.handleCloseSuccess()}
									agreeText="Ok"
								/>

								<MPDialog
									open={fail}
									title="Gagal"
									content={msg}
									styleOnAgree={{ color: '#e16764' }}
									onAgree={() => this.handleCloseSuccess()}
									agreeText="Ok"
								/>
              </CardContent>
            </Card>
          </FuseAnimate>
        </div>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(ChangePasswordPage);
