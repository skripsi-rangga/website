import {FuseLoadable} from '@fuse';

export const ForgotPasswordPageConfig = {
    settings: {
        layout: {
            config: {
                scroll : 'content',
                navbar : {
                    display : false,
                    folded  : false,
                    position: 'left'
                },
                toolbar: {
                    display : false,
                    style   : 'fixed',
                    position: 'below'
                },
                footer : {
                    display : false,
                    style   : 'fixed',
                    position: 'below'
                },
                mode   : 'fullwidth'
            }
        }
    },
    routes  : [
        {
            path     : '/pages/auth/forgot-password',
            component: FuseLoadable({
                loader: () => import('./ForgotPasswordPage')
            })
        }
    ]
};
