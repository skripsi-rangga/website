import mock from './../mock';

const reportDB = {
    reservationHistoryList:[
        {
        "reservationId":"57ae7d3-0f0b-418f-9c16-4117bfbf468f",
        "name":"aditya putra pratama",
        "numberRoom":"106",
        "roomStatus":"VC",
        "arriveDate":"2020-01-26T02:00:00",
        "departureDate":"2020-01-26T02:00:00"
        },
        {
        "reservationId":"57ae7d3-0f0b-418f-9c16-4117bfbf468f",
        "name":"putra pratama",
        "numberRoom":"108",
        "roomStatus":"VC",
        "arriveDate":"2020-02-26T02:00:00",
        "departureDate":"2020-02-26T02:00:00"
        },
    {
        "reservationId":"57ae7d3-0f0b-418f-9c16-4117bfbf468f",
        "name":"putra pratama",
        "numberRoom":"108",
        "roomStatus":"ORD",
        "arriveDate":"2020-01-27T02:00:00",
        "departureDate":"2020-01-27T02:00:00"
        },
    {
        "reservationId":"57ae7d3-0f0b-418f-9c16-4117bfbf468f",
        "name":"putra",
        "numberRoom":"109",
        "roomStatus":"VC",
        "arriveDate":"2020-01-28T02:00:00",
        "departureDate":"2020-01-28T02:00:00"
        }
    ,
    {
        "reservationId":"57ae8d3-0f0b-418f-9c16-4117bfbf468f",
        "name":"putra pr",
        "numberRoom":"110",
        "roomStatus":"VC",
        "arriveDate":"2020-01-29T02:00:00",
        "departureDate":"2020-01-29T02:00:00"
        }
    ,
    {
        "reservationId":"57ae8d3-0f6b-418f-9c16-4117bfbf468f",
        "name":"putra paar",
        "numberRoom":"120",
        "roomStatus":"VC",
        "arriveDate":"2020-01-30T02:00:00",
        "departureDate":"2020-01-30T02:00:00"
        }
    ,
    {
        "reservationId":"57ae8d3-0f6b-418f-9c17-4117bfbf468f",
        "name":"putra paar",
        "numberRoom":"120",
        "roomStatus":"VC",
        "arriveDate":"2020-01-30T02:00:00",
        "departureDate":"2020-01-30T02:00:00"
        },
        {
            "reservationId":"57ae8d3-0f6b-415f-9c16-4117bfbf468f",
            "name":"putra paar",
            "numberRoom":"120",
            "roomStatus":"VC",
            "arriveDate":"2020-01-30T02:00:00",
            "departureDate":"2020-01-30T02:00:00"
            },
            {
                "reservationId":"57ae82d3-0ft6b-415f-9c16-4117bfbf468f",
                "name":"putra paar",
                "numberRoom":"120",
                "roomStatus":"VC",
                "arriveDate":"2020-01-30T02:00:00",
                "departureDate":"2020-01-30T02:00:00"
                },
                {
                    "reservationId":"57ae8td3-0f6b-415f-9c16-4117bfbf468f",
                    "name":"putra paar",
                    "numberRoom":"120",
                    "roomStatus":"VC",
                    "arriveDate":"2020-01-30T02:00:00",
                    "departureDate":"2020-01-30T02:00:00"
                    }
    ]
};

mock.onGet('/api/report/list').reply(() => {
    return [200, reportDB.reservationHistoryList];
});
