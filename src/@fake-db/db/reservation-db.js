import mock from './../mock';
import _ from '@lodash';

const customerDB = {
    customers: [
        {
            'id'              : '1',
            'BookingId'       : '1037889217',
            'name'            : 'Agi',
            'handle'          : '1037889217',
            'checkIn'         : '23-11-2019',
            'checkOut'        : '24-11-2019',
            'room_type'       : ['Standar Room'],
            'allocation'      : [
                '101',
                '102',
                '103',
            ],
            'number_of_room'  : '3',
            'NoGuest'         : '6',
            'room_rates'      : '306881',
            'extra_bed_rates' : '0',
            'subcharge_rates' : '0',
            'total'           : '920643',
            'remark'          : 'Connecting Rooms',
            'reserved_by'     : 'OTA',
            'reserved_by_name': 'Traveloka',
            'address'         : 'Jl Budi Swadaya',
            'country'         : 'Indonesia',
            'city'            : 'Jakarta',
            'province'        : 'DKI Jakarta',
            'sub_disctric'    : 'Kebon Jeruk',
            'handphone'       : '089622874435',
            'email'           : 'agi@gmail.com',
        },
    ]
};

mock.onGet('/api/reservation/customers').reply(() => {
    return [200, customerDB.customers];
});

mock.onGet('/api/reservation/customesssr').reply((request) => {
    const {customerId} = request.params;
    const response = _.find(customerDB.customers, {id: customerId});
    return [200, response];
});

mock.onPost('/api/reservation/customer/save').reply((request) => {
    const data = JSON.parse(request.data);
    let customer = null;

    customerDB.customers = customerDB.customers.map(_product => {
        if ( _product.id === data.id )
        {
            customer = data;
            return customer
        }
        return _product;
    });

    if ( !customer )
    {
      customer = data;
      customerDB.customers = [
            ...customerDB.customers,
            customer
        ]
    }

    return [200, customer];
});

mock.onGet('/api/e-commerce-app/orders').reply(() => {
    return [200, customerDB.orders];
});

mock.onGet('/api/e-commerce-app/order').reply((request) => {
    const {orderId} = request.params;
    const response = _.find(customerDB.orders, {'id': orderId});
    return [200, response];
});
