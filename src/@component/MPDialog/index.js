import React from 'react';
import { Button } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import * as PropTypes from 'prop-types';

const MPDialog = ({
  open,
  onClose,
  title,
  content,
  disagreeText,
  onDisagree,
  styleOnDisagree,
  agreeText,
  onAgree,
  styleOnAgree,
  fullWidth,
  maxWidth,
  colorTitle,
}) => (
  <Dialog
    open={open}
    onClose={onClose}
    aria-labelledby="alert-dialog-title"
    aria-describedby="alert-dialog-description"
    fullWidth={fullWidth}
    maxWidth={maxWidth}
   
  >
    <DialogTitle id="alert-dialog-title"><b style={{fontSize:24, color:colorTitle}}>{title}</b></DialogTitle>
    <DialogContent>
      <DialogContentText  id="alert-dialog-description">
        <b style={{fontSize:18, color:'black'}}>{content}</b>
      </DialogContentText>
    </DialogContent>
    <DialogActions>
      <Button style={styleOnDisagree} onClick={onDisagree}>
        <b>{disagreeText}</b>
      </Button>
      <Button style={styleOnAgree} onClick={onAgree} color="primary" autoFocus>
        <b>{agreeText}</b>
      </Button>
    </DialogActions>
  </Dialog>
);

MPDialog.defaultProps = {
  open: false,
  onClose: () => {},
  title: '',
  content: '',
  disagreeText: '',
  onDisagree: () => {},
  agreeText: '',
  onAgree: () => {},
};

MPDialog.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  title: PropTypes.string,
  content: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  disagreeText: PropTypes.string,
  onDisagree: PropTypes.func,
  agreeText: PropTypes.string,
  onAgree: PropTypes.func,
};

export default MPDialog;
