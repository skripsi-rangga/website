import React from 'react';
import { Icon } from '@material-ui/core';
import { Link } from "react-router-dom";
import * as PropTypes from 'prop-types';


const BackLink = ({
  url,
}) => (
  <div style={{fontSize:18}}>
      <Icon
          style={{color:'black', marginRight:10}}
          component={Link}
          to={url}>
          keyboard_backspace
      </Icon>
      <span style={{position:'absolute'}}>Back</span>
  </div>
);

BackLink.defaultProps = {
  url: '',
};

BackLink.propTypes = {
  url: PropTypes.string,
};

export default BackLink;
