import React from 'react';
import { MenuItem, Grid } from "@material-ui/core";
import Formsy from "formsy-react";
import { TextFieldFormsy, SelectFormsy, FuseChipSelect } from "@fuse";


const CalculationForm = ({
    funcHandleChangeOperator,
    operatorVariabel,
    operatorName,
    operatorLabel,
    funcHandleChangeOperatorType,
    operatorTypeVariabel,
    operatorTypeLabel,
    operatorTypeName,
    funcHandleChangeVariabel,
    variabel,
    masterDataName,
    funcHandleTextForm,
    optionsData
}) => (
  <>
    <Grid item xs={3} className="pr-24">
        <div>
            <SelectFormsy
                onChange={funcHandleChangeOperator}
                className="w-full my-8"
                name={operatorName}
                label={operatorLabel}
                value={operatorVariabel}
                variant="outlined"
                autoWidth
                InputLabelProps={{ shrink: true }}
                required
            >
                <MenuItem key="1" value="plus">
                    Plus/Addition (+)
                </MenuItem>
                <MenuItem key="2" value="minus">
                    Minus/Subtraction (-)
                </MenuItem>
                <MenuItem key="3" value="division">
                    Division (/)
                </MenuItem>
                <MenuItem key="4" value="multiplication">
                    Multiplication (x)
                </MenuItem>
            </SelectFormsy>
        </div>
    </Grid>
    <Grid item xs={3} className="pr-24">
        <div>
            <SelectFormsy
                onChange={funcHandleChangeOperatorType}
                className="w-full my-8"
                name={operatorTypeName}
                label={operatorTypeLabel}
                value={operatorTypeVariabel}
                variant="outlined"
                autoWidth
                InputLabelProps={{ shrink: true }}
                required
            >
                <MenuItem key="MASTER_DATA" value="MASTER_DATA">
                    MASTER_DATA
                </MenuItem>
                <MenuItem key="CONSTANT" value="CONSTANT">
                    CONSTANT
                </MenuItem>
            </SelectFormsy>
        </div>
    </Grid>
    <Grid item xs={6} className="pr-24">
        {operatorTypeVariabel === "MASTER_DATA" ? (
            <div>
                <FuseChipSelect
                    className="w-full my-8"
                    value={variabel}
                    onChange={funcHandleChangeVariabel}
                    placeholder=""
                    textFieldProps={{
                        label: "Select Master Data",
                        InputLabelProps: {
                            shrink: true,
                        },
                        variant: "outlined",
                    }}
                    options={optionsData}
                    name={masterDataName}
                />
            </div>
        ) : (
            <div>
                <TextFieldFormsy
                    style={{ color: "red" }}
                    className="my-8"
                    label="Masukkan Nilai"
                    type="text"
                    name={masterDataName}
                    autoComplete="off"
                    variant="outlined"
                    placeholder=""
                    fullWidth
                    InputLabelProps={{ shrink: true }}
                    onChange={funcHandleTextForm}
                />
            </div>
        )}
    </Grid>
  </>
);

export default CalculationForm;
