# Stage 1 - the build process
FROM node:10.15.3 as build-deps
WORKDIR /app
COPY package.json ./
RUN yarn
COPY . ./
RUN yarn build

# Stage 2 - the production environment
FROM nginx
COPY --from=build-deps /app/build /usr/share/nginx/html
COPY default.conf /etc/nginx/conf.d/default.conf